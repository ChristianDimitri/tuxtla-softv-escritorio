﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepProrroga
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbPagoOrden = New System.Windows.Forms.GroupBox()
        Me.rbCon = New System.Windows.Forms.RadioButton()
        Me.rbSin = New System.Windows.Forms.RadioButton()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbFechas = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.gbPagoOrden.SuspendLayout()
        Me.gbFechas.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbPagoOrden
        '
        Me.gbPagoOrden.Controls.Add(Me.rbCon)
        Me.gbPagoOrden.Controls.Add(Me.rbSin)
        Me.gbPagoOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPagoOrden.Location = New System.Drawing.Point(17, 12)
        Me.gbPagoOrden.Name = "gbPagoOrden"
        Me.gbPagoOrden.Size = New System.Drawing.Size(296, 93)
        Me.gbPagoOrden.TabIndex = 0
        Me.gbPagoOrden.TabStop = False
        Me.gbPagoOrden.Text = "Reporte"
        '
        'rbCon
        '
        Me.rbCon.AutoSize = True
        Me.rbCon.Location = New System.Drawing.Point(30, 54)
        Me.rbCon.Name = "rbCon"
        Me.rbCon.Size = New System.Drawing.Size(86, 19)
        Me.rbCon.TabIndex = 1
        Me.rbCon.Text = "Con pago"
        Me.rbCon.UseVisualStyleBackColor = True
        '
        'rbSin
        '
        Me.rbSin.AutoSize = True
        Me.rbSin.Checked = True
        Me.rbSin.Location = New System.Drawing.Point(30, 29)
        Me.rbSin.Name = "rbSin"
        Me.rbSin.Size = New System.Drawing.Size(82, 19)
        Me.rbSin.TabIndex = 0
        Me.rbSin.TabStop = True
        Me.rbSin.Text = "Sin pago"
        Me.rbSin.UseVisualStyleBackColor = True
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(23, 225)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 2
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(165, 225)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 3
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'gbFechas
        '
        Me.gbFechas.Controls.Add(Me.Label2)
        Me.gbFechas.Controls.Add(Me.Label1)
        Me.gbFechas.Controls.Add(Me.dtpFechaFin)
        Me.gbFechas.Controls.Add(Me.dtpFechaIni)
        Me.gbFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFechas.Location = New System.Drawing.Point(17, 111)
        Me.gbFechas.Name = "gbFechas"
        Me.gbFechas.Size = New System.Drawing.Size(296, 93)
        Me.gbFechas.TabIndex = 1
        Me.gbFechas.TabStop = False
        Me.gbFechas.Text = "Fechas de Prórroga"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(80, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "al"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(72, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "del"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.Location = New System.Drawing.Point(105, 51)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(103, 21)
        Me.dtpFechaFin.TabIndex = 1
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.Location = New System.Drawing.Point(105, 24)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(103, 21)
        Me.dtpFechaIni.TabIndex = 0
        '
        'FrmRepProrroga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(325, 291)
        Me.Controls.Add(Me.gbFechas)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.gbPagoOrden)
        Me.Name = "FrmRepProrroga"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Promesas de Pago"
        Me.gbPagoOrden.ResumeLayout(False)
        Me.gbPagoOrden.PerformLayout()
        Me.gbFechas.ResumeLayout(False)
        Me.gbFechas.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPagoOrden As System.Windows.Forms.GroupBox
    Friend WithEvents rbCon As System.Windows.Forms.RadioButton
    Friend WithEvents rbSin As System.Windows.Forms.RadioButton
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents gbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
End Class
