﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelAlmacenUsu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxUsuario = New System.Windows.Forms.ComboBox()
        Me.ComboBoxAlmacen = New System.Windows.Forms.ComboBox()
        Me.Rel_almacenusu = New System.Windows.Forms.DataGridView()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clv_usuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdAlmacenEmpresa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.Rel_almacenusu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel7.Location = New System.Drawing.Point(30, 25)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(72, 18)
        Me.CMBLabel7.TabIndex = 407
        Me.CMBLabel7.Text = "Usuario:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(320, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 18)
        Me.Label1.TabIndex = 408
        Me.Label1.Text = "Almacén:"
        '
        'ComboBoxUsuario
        '
        Me.ComboBoxUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxUsuario.ForeColor = System.Drawing.Color.Red
        Me.ComboBoxUsuario.FormattingEnabled = True
        Me.ComboBoxUsuario.Location = New System.Drawing.Point(33, 56)
        Me.ComboBoxUsuario.Name = "ComboBoxUsuario"
        Me.ComboBoxUsuario.Size = New System.Drawing.Size(268, 26)
        Me.ComboBoxUsuario.TabIndex = 409
        Me.ComboBoxUsuario.TabStop = False
        '
        'ComboBoxAlmacen
        '
        Me.ComboBoxAlmacen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBoxAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxAlmacen.ForeColor = System.Drawing.Color.Red
        Me.ComboBoxAlmacen.FormattingEnabled = True
        Me.ComboBoxAlmacen.Location = New System.Drawing.Point(323, 56)
        Me.ComboBoxAlmacen.Name = "ComboBoxAlmacen"
        Me.ComboBoxAlmacen.Size = New System.Drawing.Size(268, 26)
        Me.ComboBoxAlmacen.TabIndex = 410
        Me.ComboBoxAlmacen.TabStop = False
        '
        'Rel_almacenusu
        '
        Me.Rel_almacenusu.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Rel_almacenusu.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Rel_almacenusu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Rel_almacenusu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.clv_usuario, Me.nombre, Me.IdAlmacenEmpresa, Me.descripcion})
        Me.Rel_almacenusu.Location = New System.Drawing.Point(33, 106)
        Me.Rel_almacenusu.Name = "Rel_almacenusu"
        Me.Rel_almacenusu.ReadOnly = True
        Me.Rel_almacenusu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Rel_almacenusu.Size = New System.Drawing.Size(558, 210)
        Me.Rel_almacenusu.TabIndex = 411
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(607, 54)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(105, 29)
        Me.Button3.TabIndex = 433
        Me.Button3.TabStop = False
        Me.Button3.Text = "&Agregar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(607, 106)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 29)
        Me.Button1.TabIndex = 434
        Me.Button1.TabStop = False
        Me.Button1.Text = "&Eliminar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'id
        '
        Me.id.DataPropertyName = "id"
        Me.id.HeaderText = "ID"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Width = 50
        '
        'clv_usuario
        '
        Me.clv_usuario.DataPropertyName = "clv_usuario"
        Me.clv_usuario.HeaderText = "clv_usuario"
        Me.clv_usuario.Name = "clv_usuario"
        Me.clv_usuario.ReadOnly = True
        Me.clv_usuario.Visible = False
        '
        'nombre
        '
        Me.nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombre.DataPropertyName = "nombre"
        Me.nombre.HeaderText = "USUARIO"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'IdAlmacenEmpresa
        '
        Me.IdAlmacenEmpresa.DataPropertyName = "IdAlmacenEmpresa"
        Me.IdAlmacenEmpresa.HeaderText = "IdAlmacenEmpresa"
        Me.IdAlmacenEmpresa.Name = "IdAlmacenEmpresa"
        Me.IdAlmacenEmpresa.ReadOnly = True
        Me.IdAlmacenEmpresa.Visible = False
        '
        'descripcion
        '
        Me.descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.descripcion.DataPropertyName = "descripcion"
        Me.descripcion.HeaderText = "ALMACEN"
        Me.descripcion.Name = "descripcion"
        Me.descripcion.ReadOnly = True
        '
        'FrmRelAlmacenUsu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(719, 334)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Rel_almacenusu)
        Me.Controls.Add(Me.ComboBoxAlmacen)
        Me.Controls.Add(Me.ComboBoxUsuario)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Name = "FrmRelAlmacenUsu"
        Me.Text = "Relación Almacén Usuarios"
        CType(Me.Rel_almacenusu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxAlmacen As System.Windows.Forms.ComboBox
    Friend WithEvents Rel_almacenusu As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_usuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdAlmacenEmpresa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
