<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim ContratonetLabel As System.Windows.Forms.Label
        Dim CLV_CABLEMODEMLabel As System.Windows.Forms.Label
        Dim Maccablemodem1Label As System.Windows.Forms.Label
        Dim CLV_CABLEMODEMNewLabel As System.Windows.Forms.Label
        Dim Maccablemodem2Label As System.Windows.Forms.Label
        Dim NoFuncionaLabel As System.Windows.Forms.Label
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.CONCAPARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONCAPARTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCAPARTableAdapter
        Me.CONCAPARBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONCAPARBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.FillToolStrip = New System.Windows.Forms.ToolStrip
        Me.ClaveToolStripLabel = New System.Windows.Forms.ToolStripLabel
        Me.ClaveToolStripTextBox = New System.Windows.Forms.ToolStripTextBox
        Me.Clv_OrdenToolStripLabel = New System.Windows.Forms.ToolStripLabel
        Me.Clv_OrdenToolStripTextBox = New System.Windows.Forms.ToolStripTextBox
        Me.ContratonetToolStripLabel = New System.Windows.Forms.ToolStripLabel
        Me.ContratonetToolStripTextBox = New System.Windows.Forms.ToolStripTextBox
        Me.FillToolStripButton = New System.Windows.Forms.ToolStripButton
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox
        Me.ContratonetTextBox = New System.Windows.Forms.TextBox
        Me.CLV_CABLEMODEMTextBox = New System.Windows.Forms.TextBox
        Me.Maccablemodem1TextBox = New System.Windows.Forms.TextBox
        Me.CLV_CABLEMODEMNewTextBox = New System.Windows.Forms.TextBox
        Me.Maccablemodem2TextBox = New System.Windows.Forms.TextBox
        Me.NoFuncionaCheckBox = New System.Windows.Forms.CheckBox
        ClaveLabel = New System.Windows.Forms.Label
        Clv_OrdenLabel = New System.Windows.Forms.Label
        ContratonetLabel = New System.Windows.Forms.Label
        CLV_CABLEMODEMLabel = New System.Windows.Forms.Label
        Maccablemodem1Label = New System.Windows.Forms.Label
        CLV_CABLEMODEMNewLabel = New System.Windows.Forms.Label
        Maccablemodem2Label = New System.Windows.Forms.Label
        NoFuncionaLabel = New System.Windows.Forms.Label
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCAPARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONCAPARBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCAPARBindingNavigator.SuspendLayout()
        Me.FillToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONCAPARBindingSource
        '
        Me.CONCAPARBindingSource.DataMember = "CONCAPAR"
        Me.CONCAPARBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONCAPARTableAdapter
        '
        Me.CONCAPARTableAdapter.ClearBeforeFill = True
        '
        'CONCAPARBindingNavigator
        '
        Me.CONCAPARBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.CONCAPARBindingNavigator.BindingSource = Me.CONCAPARBindingSource
        Me.CONCAPARBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.CONCAPARBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCAPARBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.CONCAPARBindingNavigatorSaveItem})
        Me.CONCAPARBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCAPARBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.CONCAPARBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.CONCAPARBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.CONCAPARBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.CONCAPARBindingNavigator.Name = "CONCAPARBindingNavigator"
        Me.CONCAPARBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.CONCAPARBindingNavigator.Size = New System.Drawing.Size(635, 25)
        Me.CONCAPARBindingNavigator.TabIndex = 0
        Me.CONCAPARBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(38, 13)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'CONCAPARBindingNavigatorSaveItem
        '
        Me.CONCAPARBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.CONCAPARBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCAPARBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCAPARBindingNavigatorSaveItem.Name = "CONCAPARBindingNavigatorSaveItem"
        Me.CONCAPARBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 20)
        Me.CONCAPARBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'FillToolStrip
        '
        Me.FillToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClaveToolStripLabel, Me.ClaveToolStripTextBox, Me.Clv_OrdenToolStripLabel, Me.Clv_OrdenToolStripTextBox, Me.ContratonetToolStripLabel, Me.ContratonetToolStripTextBox, Me.FillToolStripButton})
        Me.FillToolStrip.Location = New System.Drawing.Point(0, 25)
        Me.FillToolStrip.Name = "FillToolStrip"
        Me.FillToolStrip.Size = New System.Drawing.Size(635, 25)
        Me.FillToolStrip.TabIndex = 1
        Me.FillToolStrip.Text = "FillToolStrip"
        '
        'ClaveToolStripLabel
        '
        Me.ClaveToolStripLabel.Name = "ClaveToolStripLabel"
        Me.ClaveToolStripLabel.Size = New System.Drawing.Size(38, 22)
        Me.ClaveToolStripLabel.Text = "Clave:"
        '
        'ClaveToolStripTextBox
        '
        Me.ClaveToolStripTextBox.Name = "ClaveToolStripTextBox"
        Me.ClaveToolStripTextBox.Size = New System.Drawing.Size(100, 21)
        '
        'Clv_OrdenToolStripLabel
        '
        Me.Clv_OrdenToolStripLabel.Name = "Clv_OrdenToolStripLabel"
        Me.Clv_OrdenToolStripLabel.Size = New System.Drawing.Size(62, 13)
        Me.Clv_OrdenToolStripLabel.Text = "Clv_Orden:"
        '
        'Clv_OrdenToolStripTextBox
        '
        Me.Clv_OrdenToolStripTextBox.Name = "Clv_OrdenToolStripTextBox"
        Me.Clv_OrdenToolStripTextBox.Size = New System.Drawing.Size(100, 21)
        '
        'ContratonetToolStripLabel
        '
        Me.ContratonetToolStripLabel.Name = "ContratonetToolStripLabel"
        Me.ContratonetToolStripLabel.Size = New System.Drawing.Size(70, 13)
        Me.ContratonetToolStripLabel.Text = "Contratonet:"
        '
        'ContratonetToolStripTextBox
        '
        Me.ContratonetToolStripTextBox.Name = "ContratonetToolStripTextBox"
        Me.ContratonetToolStripTextBox.Size = New System.Drawing.Size(100, 21)
        '
        'FillToolStripButton
        '
        Me.FillToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FillToolStripButton.Name = "FillToolStripButton"
        Me.FillToolStripButton.Size = New System.Drawing.Size(23, 17)
        Me.FillToolStripButton.Text = "Fill"
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Location = New System.Drawing.Point(129, 109)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(37, 13)
        ClaveLabel.TabIndex = 2
        ClaveLabel.Text = "Clave:"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(265, 106)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(104, 20)
        Me.ClaveTextBox.TabIndex = 3
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Location = New System.Drawing.Point(129, 135)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(57, 13)
        Clv_OrdenLabel.TabIndex = 4
        Clv_OrdenLabel.Text = "Clv Orden:"
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(265, 132)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(104, 20)
        Me.Clv_OrdenTextBox.TabIndex = 5
        '
        'ContratonetLabel
        '
        ContratonetLabel.AutoSize = True
        ContratonetLabel.Location = New System.Drawing.Point(129, 161)
        ContratonetLabel.Name = "ContratonetLabel"
        ContratonetLabel.Size = New System.Drawing.Size(65, 13)
        ContratonetLabel.TabIndex = 6
        ContratonetLabel.Text = "Contratonet:"
        '
        'ContratonetTextBox
        '
        Me.ContratonetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Contratonet", True))
        Me.ContratonetTextBox.Location = New System.Drawing.Point(265, 158)
        Me.ContratonetTextBox.Name = "ContratonetTextBox"
        Me.ContratonetTextBox.Size = New System.Drawing.Size(104, 20)
        Me.ContratonetTextBox.TabIndex = 7
        '
        'CLV_CABLEMODEMLabel
        '
        CLV_CABLEMODEMLabel.AutoSize = True
        CLV_CABLEMODEMLabel.Location = New System.Drawing.Point(129, 187)
        CLV_CABLEMODEMLabel.Name = "CLV_CABLEMODEMLabel"
        CLV_CABLEMODEMLabel.Size = New System.Drawing.Size(108, 13)
        CLV_CABLEMODEMLabel.TabIndex = 8
        CLV_CABLEMODEMLabel.Text = "CLV CABLEMODEM:"
        '
        'CLV_CABLEMODEMTextBox
        '
        Me.CLV_CABLEMODEMTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "CLV_CABLEMODEM", True))
        Me.CLV_CABLEMODEMTextBox.Location = New System.Drawing.Point(265, 184)
        Me.CLV_CABLEMODEMTextBox.Name = "CLV_CABLEMODEMTextBox"
        Me.CLV_CABLEMODEMTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CLV_CABLEMODEMTextBox.TabIndex = 9
        '
        'Maccablemodem1Label
        '
        Maccablemodem1Label.AutoSize = True
        Maccablemodem1Label.Location = New System.Drawing.Point(129, 213)
        Maccablemodem1Label.Name = "Maccablemodem1Label"
        Maccablemodem1Label.Size = New System.Drawing.Size(97, 13)
        Maccablemodem1Label.TabIndex = 10
        Maccablemodem1Label.Text = "Maccablemodem1:"
        '
        'Maccablemodem1TextBox
        '
        Me.Maccablemodem1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Maccablemodem1", True))
        Me.Maccablemodem1TextBox.Location = New System.Drawing.Point(265, 210)
        Me.Maccablemodem1TextBox.Name = "Maccablemodem1TextBox"
        Me.Maccablemodem1TextBox.Size = New System.Drawing.Size(104, 20)
        Me.Maccablemodem1TextBox.TabIndex = 11
        '
        'CLV_CABLEMODEMNewLabel
        '
        CLV_CABLEMODEMNewLabel.AutoSize = True
        CLV_CABLEMODEMNewLabel.Location = New System.Drawing.Point(129, 239)
        CLV_CABLEMODEMNewLabel.Name = "CLV_CABLEMODEMNewLabel"
        CLV_CABLEMODEMNewLabel.Size = New System.Drawing.Size(130, 13)
        CLV_CABLEMODEMNewLabel.TabIndex = 12
        CLV_CABLEMODEMNewLabel.Text = "CLV CABLEMODEMNew:"
        '
        'CLV_CABLEMODEMNewTextBox
        '
        Me.CLV_CABLEMODEMNewTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "CLV_CABLEMODEMNew", True))
        Me.CLV_CABLEMODEMNewTextBox.Location = New System.Drawing.Point(265, 236)
        Me.CLV_CABLEMODEMNewTextBox.Name = "CLV_CABLEMODEMNewTextBox"
        Me.CLV_CABLEMODEMNewTextBox.Size = New System.Drawing.Size(104, 20)
        Me.CLV_CABLEMODEMNewTextBox.TabIndex = 13
        '
        'Maccablemodem2Label
        '
        Maccablemodem2Label.AutoSize = True
        Maccablemodem2Label.Location = New System.Drawing.Point(129, 265)
        Maccablemodem2Label.Name = "Maccablemodem2Label"
        Maccablemodem2Label.Size = New System.Drawing.Size(97, 13)
        Maccablemodem2Label.TabIndex = 14
        Maccablemodem2Label.Text = "Maccablemodem2:"
        '
        'Maccablemodem2TextBox
        '
        Me.Maccablemodem2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAPARBindingSource, "Maccablemodem2", True))
        Me.Maccablemodem2TextBox.Location = New System.Drawing.Point(265, 262)
        Me.Maccablemodem2TextBox.Name = "Maccablemodem2TextBox"
        Me.Maccablemodem2TextBox.Size = New System.Drawing.Size(104, 20)
        Me.Maccablemodem2TextBox.TabIndex = 15
        '
        'NoFuncionaLabel
        '
        NoFuncionaLabel.AutoSize = True
        NoFuncionaLabel.Location = New System.Drawing.Point(129, 293)
        NoFuncionaLabel.Name = "NoFuncionaLabel"
        NoFuncionaLabel.Size = New System.Drawing.Size(71, 13)
        NoFuncionaLabel.TabIndex = 16
        NoFuncionaLabel.Text = "No Funciona:"
        '
        'NoFuncionaCheckBox
        '
        Me.NoFuncionaCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONCAPARBindingSource, "NoFunciona", True))
        Me.NoFuncionaCheckBox.Location = New System.Drawing.Point(265, 288)
        Me.NoFuncionaCheckBox.Name = "NoFuncionaCheckBox"
        Me.NoFuncionaCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.NoFuncionaCheckBox.TabIndex = 17
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(635, 433)
        Me.Controls.Add(ClaveLabel)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Clv_OrdenLabel)
        Me.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Controls.Add(ContratonetLabel)
        Me.Controls.Add(Me.ContratonetTextBox)
        Me.Controls.Add(CLV_CABLEMODEMLabel)
        Me.Controls.Add(Me.CLV_CABLEMODEMTextBox)
        Me.Controls.Add(Maccablemodem1Label)
        Me.Controls.Add(Me.Maccablemodem1TextBox)
        Me.Controls.Add(CLV_CABLEMODEMNewLabel)
        Me.Controls.Add(Me.CLV_CABLEMODEMNewTextBox)
        Me.Controls.Add(Maccablemodem2Label)
        Me.Controls.Add(Me.Maccablemodem2TextBox)
        Me.Controls.Add(NoFuncionaLabel)
        Me.Controls.Add(Me.NoFuncionaCheckBox)
        Me.Controls.Add(Me.FillToolStrip)
        Me.Controls.Add(Me.CONCAPARBindingNavigator)
        Me.Name = "Form2"
        Me.Text = "Form2"
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCAPARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONCAPARBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCAPARBindingNavigator.ResumeLayout(False)
        Me.CONCAPARBindingNavigator.PerformLayout()
        Me.FillToolStrip.ResumeLayout(False)
        Me.FillToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCAPARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCAPARTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCAPARTableAdapter
    Friend WithEvents CONCAPARBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONCAPARBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents FillToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents ClaveToolStripLabel As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ClaveToolStripTextBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Clv_OrdenToolStripLabel As System.Windows.Forms.ToolStripLabel
    Friend WithEvents Clv_OrdenToolStripTextBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ContratonetToolStripLabel As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ContratonetToolStripTextBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents FillToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratonetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLV_CABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Maccablemodem1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLV_CABLEMODEMNewTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Maccablemodem2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoFuncionaCheckBox As System.Windows.Forms.CheckBox
End Class
