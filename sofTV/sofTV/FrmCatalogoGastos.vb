﻿Public Class FrmCatalogoGastos
    ''SE DECLARAN LAS VARIABLES QUE SE VAN A OCUPAR DURANTE EL PROCESO DE ALMACENAMIENTO O MODIFICACIÓN
    Public opcionGastos As String = Nothing
    Public idTipoGasto As Long


    Private Sub FrmCatalogoGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name) ''SE INVOCA EL COLOREA PARA DARLE FORMATO A LOS CONTROLES

        If opcionGastos = "C" Or opcionGastos = "M" Then ''SI ES UNA CONSULTA O UNA MODIFICACIÓN DE OBTIENEN LOS DATOS
            uspConsultaTipoGastos(idTipoGasto, String.Empty, 0, 1)
        End If

        If opcionGastos = "C" Then ''SI ES UNA CONSULTA SE PCULTAN LOS CONTROLES Y SÓLO SE MUETRA LA INFORMACIÓN
            Me.txtDescripcion.Enabled = False
            Me.cbActivo.Enabled = False
            Me.btnGuadar.Enabled = False
        ElseIf opcionGastos = "N" Or opcionGastos = "M" Then ''SI ES UN NUEVO REGISTRO O UNA MODIFICACIÓN SE HABILITAN LOS CAMPOS
            Me.txtDescripcion.Enabled = True
            Me.cbActivo.Enabled = True
            Me.btnGuadar.Enabled = True
        End If
    End Sub
    Private Sub uspInsertaTipoGastos(ByVal prmDescripcion As String, ByVal prmActivo As Boolean)
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A INSERTAR LOS DATOS (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@activo", SqlDbType.Bit, prmActivo)

        If ControlEfectivoClass.ConsultaBol("uspInsertaTipoGastos") = False Then
            MsgBox("El Registro que quiere Ingresar ya se Encuentra en la Lista", MsgBoxStyle.Information)
        Else
            MsgBox("Registro Almacenado Exitosamente", MsgBoxStyle.Information)
        End If
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A INSERTAR LOS DATOS (FIN)
    End Sub

    Private Sub uspModoficaTipoGastos(ByVal prmIdTipoGasto As String, ByVal prmDescripcion As String, ByVal prmActivo As Boolean)
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A MODIFICAR LOS DATOS (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.VarChar, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@descripcion", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@activo", SqlDbType.Bit, prmActivo)

        If ControlEfectivoClass.ConsultaBol("uspModoficaTipoGastos") = False Then
            MsgBox("El Registro ya se Encuentra en la Lista", MsgBoxStyle.Information)
        Else
            MsgBox("Registro Modificado Exitosamente", MsgBoxStyle.Information)
        End If
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A MODIFICAR LOS DATOS (FIN)
    End Sub

    Private Sub uspConsultaTipoGastos(ByVal prmidTipoGasto As Long, ByVal prmDescripcion As String, ByVal prmActivo As Boolean, ByVal prmOp As Integer)
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A MODIFICAR LOS DATOS (INICIO)
        Dim DT As New DataTable
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idTipoGasto", SqlDbType.BigInt, prmIdTipoGasto)
        ControlEfectivoClass.CreateMyParameter("@DESCRIPCION", SqlDbType.VarChar, prmDescripcion, 250)
        ControlEfectivoClass.CreateMyParameter("@ACTIVO", SqlDbType.Bit, prmActivo)
        ControlEfectivoClass.CreateMyParameter("@OP", SqlDbType.Int, prmOp)

        DT = ControlEfectivoClass.ConsultaDT("uspConsultaTipoGastos")
        Me.txtDescripcion.Text = DT.Rows(0)("DESCRIPCION").ToString()
        Me.cbActivo.Checked = CBool(DT.Rows(0)("ACTIVO").ToString())
        '''''MANDAMOS EJECUTAR EL PROCEDIMIENTO QUE VA A MODIFICAR LOS DATOS (FIN)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close() 'SE CIERRA LA VENTANA
    End Sub

    Private Sub btnGuadar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuadar.Click
        If Me.txtDescripcion.Text.Length = 0 Then ''SE CALIDA QUE EL TEXT BOS DE LA DESCRIPCIÓN CONTENGA TEXTO
            MsgBox("Ingrese una Descripción Válida", MsgBoxStyle.Information)
            Exit Sub
        End If

        If opcionGastos = "N" Then ''VALIDA SI ES NUEVO MANDA EL PROCEDIMIENTO DE INSERCIÓN DE NUEVOS REGISTROS
            uspInsertaTipoGastos(Me.txtDescripcion.Text, Me.cbActivo.Checked)
        ElseIf opcionGastos = "M" Then ''VALIDA SI ES NIDIFICAR MANDA EL PROCEDIMIENTO QUE MODIFICA REGISTROS
            uspModoficaTipoGastos(idTipoGasto, Me.txtDescripcion.Text, Me.cbActivo.Checked)
        End If
        Me.Close()
    End Sub
End Class