﻿Public Class FrmCambiaClientePlaca
    Public contratoPlaca As Long
    Dim ClientePlaca As New ClassCambioClientePlaca

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.CLOSE()
    End Sub

    Private Sub btnCambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambiar.Click
        If Me.txtPlacaNueva.Text.Length = 0 Then
            MsgBox("¡Capture una Placa Válida!", MsgBoxStyle.Information)
            Exit Sub
        End If

        'If IsNumeric(Me.txtPlacaNueva.Text) = True Then
        uspActualizaClientePlaca(contratoPlaca, Me.txtPlacaNueva.Text)
        MsgBox("Cliente Actualizado Satisfactoriamente", MsgBoxStyle.Information)
        Me.Close()
        'Else
        'MsgBox("La Nueva Placa sólo admite caracteres numércios", MsgBoxStyle.Information)
        'Exit Sub
        'End If
    End Sub

    Private Sub dameDatosCliente(ByVal prmContrato As Long)
        ClientePlaca.uspDameDatosClientePlaca(prmContrato)
        Me.txtContrato.Text = ClientePlaca.contratoPlaca
        Me.txtNombre.Text = ClientePlaca.nombrePlaca
        Me.txtCalle.Text = ClientePlaca.callePlaca
        Me.txtNumero.Text = ClientePlaca.numeroPlaca
        Me.txtColonia.Text = ClientePlaca.coloniaPlaca
        Me.txtStatus.Text = ClientePlaca.statusPlaca
        Me.txtPeriodoPagado.Text = ClientePlaca.periodoPlaca
        Me.txtPlacaActual.Text = ClientePlaca.placaActualPlaca
    End Sub

    Private Sub FrmCambiaClientePlaca_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        dameDatosCliente(contratoPlaca)
    End Sub

    Private Sub uspActualizaClientePlaca(ByVal prmContrato As Long, ByVal prmPlaca As String)
        ClientePlaca.uspActualizaClientePlaca(prmContrato, prmPlaca)
    End Sub
End Class