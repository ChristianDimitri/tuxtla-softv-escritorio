<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDesconexionTempo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnBuscarCliente = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 19)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(253, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Proceso de Desconexión Temporal"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(47, 66)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(70, 16)
        Me.CMBLabel2.TabIndex = 1
        Me.CMBLabel2.Text = "Contrato:"
        '
        'tbContrato
        '
        Me.tbContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(123, 58)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(135, 22)
        Me.tbContrato.TabIndex = 2
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(66, 118)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 3
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(208, 118)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 5
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnBuscarCliente
        '
        Me.bnBuscarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarCliente.Location = New System.Drawing.Point(264, 63)
        Me.bnBuscarCliente.Name = "bnBuscarCliente"
        Me.bnBuscarCliente.Size = New System.Drawing.Size(39, 19)
        Me.bnBuscarCliente.TabIndex = 6
        Me.bnBuscarCliente.Text = "..."
        Me.bnBuscarCliente.UseVisualStyleBackColor = True
        '
        'FrmDesconexionTempo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(356, 166)
        Me.Controls.Add(Me.bnBuscarCliente)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.MaximizeBox = False
        Me.Name = "FrmDesconexionTempo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desconexión Temporal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnBuscarCliente As System.Windows.Forms.Button
End Class
