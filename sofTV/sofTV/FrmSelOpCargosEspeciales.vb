Imports System.Data.SqlClient
Public Class FrmSelOpCargosEspeciales

    Private Sub FrmSelOpCargosEspeciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locpagosespeciales = False
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.MuestraTipSerPrincipal_SERTableAdapter.Connection = con
        Me.MuestraTipSerPrincipal_SERTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal_SER)
        Me.ComboBox1.Text = ""
        con.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
        FrmCargosEspeciales.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox1.Text = "" Then
            MsgBox("Seleccione Por Favor Un Tipo de Servicio", MsgBoxStyle.Information)
            Exit Sub
        End If
        GloClv_TipSer = Me.ComboBox1.SelectedValue
        locpagosespeciales = True
        Me.Close()
    End Sub
End Class