Imports System.Data.SqlClient
Public Class FrmDescargaMateTecQuejas

    Private Sub FrmDescargaMateTecQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.DimeSiTieneunaBitacora_QTableAdapter.Connection = CON
        Me.DimeSiTieneunaBitacora_QTableAdapter.Fill(Me.DataSetLidia.DimeSiTieneunaBitacora_Q, gloClave)
        Me.Muestra_Detalle_BitacoraTableAdapter.Connection = CON
        Me.Muestra_Detalle_BitacoraTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Detalle_Bitacora, Locclv_tec)


        If IsNumeric(Locclv_tec) = True And IsNumeric(ComboBox1.SelectedValue) Then
            Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON
            Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Me.ComboBox1.SelectedValue)
        End If
        CON.Close()
        llena_Detalle()
        If opcion <> "C" Then
            Me.OK.Visible = True
            Me.Button4.Visible = False
            Me.TextBox1.Enabled = True
            Me.ComboBox1.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            If gLOVERgUARDA = 1 Then
                Me.OK.Visible = False
                Me.Button4.Visible = True

            End If
        End If
        If opcion = "C" Then
            Me.OK.Visible = False
            Me.Button4.Visible = False
            Me.TextBox1.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AGREGAR()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.NoArticuloTextBox.Text) = True Then
            If Me.MovimientoTextBox.Text = "N" Then
                BorraConcepto()
            ElseIf IsNumeric(Me.NoBitacoraTextBox.Text) = True Then
                If Me.NoBitacoraTextBox.Text > 0 Then
                    BorraExistente()
                End If
            End If
            llena_Detalle()
        End If
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        LocValida1 = False
        If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
            LocValida1 = True
            'Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
            'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
            'Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))
            Me.Close()
        Else
            MsgBox("La Lista esta vacia por lo cual no se puede Guardar ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Quita_SEBORROART_SALTEC_QTableAdapter.Connection = CON
        Me.Quita_SEBORROART_SALTEC_QTableAdapter.Fill(Me.DataSetLidia.Quita_SEBORROART_SALTEC_Q, clv_sessionTecnico)
        Me.Dame_Folio_QTableAdapter.Connection = CON
        Me.Dame_Folio_QTableAdapter.Fill(Me.DataSetLidia.Dame_Folio_Q, gloClave, 1, Locclv_folio)
        Me.Inserta_Bitacora_tec_QTableAdapter.Connection = CON
        Me.Inserta_Bitacora_tec_QTableAdapter.Fill(Me.DataSetLidia.Inserta_Bitacora_tec_Q, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Connection = CON
        Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Fill(Me.DataSetLidia.Inserta_Rel_Bitacora_Queja, LocNo_Bitacora, CInt(gloClave))
        Me.Inserta_RelCobraDescTableAdapter.Connection = CON
        Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "Q")
        MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)

        Dim num As Integer
        Dim RESP As String
        CON.Open()
        Me.Revisa_Tempo_QTableAdapter.Connection = CON
        Me.Revisa_Tempo_QTableAdapter.Fill(Me.DataSetLidia.Revisa_Tempo_Q, clv_sessionTecnico, num)
        CON.Close()
        'CUANDO EL BOTON ACEPTAR ES VISIBLE=================================================
        If num > 0 And Me.Button4.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString
            If RESP = "Yes" Then
                'boton guardar
                CON.Open()
                Me.Quita_SEBORROART_SALTEC_QTableAdapter.Connection = CON
                Me.Quita_SEBORROART_SALTEC_QTableAdapter.Fill(Me.DataSetLidia.Quita_SEBORROART_SALTEC_Q, clv_sessionTecnico)
                Me.Dame_Folio_QTableAdapter.Connection = CON
                Me.Dame_Folio_QTableAdapter.Fill(Me.DataSetLidia.Dame_Folio_Q, gloClave, 1, Locclv_folio)
                Me.Inserta_Bitacora_tec_QTableAdapter.Connection = CON
                Me.Inserta_Bitacora_tec_QTableAdapter.Fill(Me.DataSetLidia.Inserta_Bitacora_tec_Q, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Connection = CON
                Me.Inserta_Rel_Bitacora_QuejaTableAdapter.Fill(Me.DataSetLidia.Inserta_Rel_Bitacora_Queja, LocNo_Bitacora, CInt(gloClave))
                CON.Close()
                MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_Tempo_QTableAdapter.Connection = CON
                Me.Borra_Inserta_Tempo_QTableAdapter.Fill(Me.DataSetLidia.Borra_Inserta_Tempo_Q, clv_sessionTecnico)
                CON.Close()
            End If
        End If
        'CUANDO EL BOTON GUARDAR ES VISIBLE==================================================
        If num > 0 And Me.OK.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString()
            If RESP = "Yes" Then
                'boton Aceptar
                LocValida1 = True
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_Tempo_QTableAdapter.Connection = CON
                Me.Borra_Inserta_Tempo_QTableAdapter.Fill(Me.DataSetLidia.Borra_Inserta_Tempo_Q, clv_sessionTecnico)
                CON.Close()
            End If
        End If

        ' Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
        Me.Close()
    End Sub
    Private Sub AGREGAR()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        Dim CON2 As New SqlConnection(MiConexion)
        Dim bndduplicados As Integer = 0
        Try
            If IsNumeric(TextBox1.Text) = True Then
                CON.Open()
                Me.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter.Connection = CON
                Me.ValidaDuplicadoRel_Session_Tecnicos_QTableAdapter.Fill(Me.DataSetLidia.ValidaDuplicadoRel_Session_Tecnicos_Q, gloClave, New System.Nullable(Of Long)(CType(clv_sessionTecnico, Long)), New System.Nullable(Of Integer)(CType(Me.TextBox2.Text, Integer)), bndduplicados)
                CON.Close()
                If bndduplicados = 0 Then
                    CON1.Open()
                    Me.ValidaExistenciasTecnicosTableAdapter.Connection = CON1
                    Me.ValidaExistenciasTecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaExistenciasTecnicos, New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.TextBox2.Text, Long)), New System.Nullable(Of Long)(CType(Me.TextBox1.Text, Long)))
                    CON1.Close()
                    If Me.RespuestaTextBox.Text = "4" Then
                        CON2.Open()
                        Me.Inserta_Valores_TempoTableAdapter.Connection = CON2
                        Me.Inserta_Valores_TempoTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Valores_Tempo, clv_sessionTecnico, CInt(Me.TextBox2.Text), CInt(Me.TextBox3.Text), 1, 1, Me.ComboBox3.Text, CInt(Me.TextBox1.Text), Locclv_tec)
                        CON2.Close()
                        Me.TextBox1.Clear()
                        llena_Detalle()
                    Else
                        Me.TextBox1.Text = ""
                        MsgBox("No tiene existencias suficientes de este material", MsgBoxStyle.Information)
                    End If
                Else
                    Me.TextBox1.Text = ""
                    MsgBox("El Articulo ya esta en la lista ", MsgBoxStyle.Information)
                End If
            Else
                Me.TextBox1.Text = ""
                MsgBox("Capture datos validos ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub llena_Detalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.ConRel_Session_Tecnicos_QTableAdapter.Connection = CON
            Me.ConRel_Session_Tecnicos_QTableAdapter.Fill(Me.DataSetLidia.ConRel_Session_Tecnicos_Q, gloClave, New System.Nullable(Of Integer)(CType(Locclv_tec, Integer)), clv_sessionTecnico)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BorraConcepto()
        
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Connection = CON
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Fill(Me.DataSetEdgarRev2.Borra_Inserta_Tempo_Por_Servicio, New System.Nullable(Of Integer)(CType(clv_sessionTecnico, Integer)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorraExistente()
        
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.INSERTA_Borra_Articulo_Bitacora_QTableAdapter.Connection = CON
            Me.INSERTA_Borra_Articulo_Bitacora_QTableAdapter.Fill(Me.DataSetLidia.INSERTA_Borra_Articulo_Bitacora_Q, clv_sessionTecnico, New System.Nullable(Of Long)(CType(gloClave, Long)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.NoBitacoraTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim bndduplicados As Integer = 0
        Try
            e.KeyChar = Chr(ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N"))
            If e.KeyChar = Chr(13) Then
                AGREGAR()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ConRel_Session_TecnicosDataGridView.Enabled = False
        Me.Panel1.Visible = True
    End Sub

    Private Sub Clv_tipo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_tipo.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Locclv_tec) = True And IsNumeric(Clv_tipo.Text) = True Then
            Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON
            Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Clv_tipo.Text)
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Click
        'If IsNumeric(ComboBox1.SelectedValue) = True Then
        'Clv_tipo.Text = Me.ComboBox1.SelectedValue
        'End If
    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Clv_tipo.Text = 0
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            If Me.ComboBox1.SelectedValue > 0 Then
                Clv_tipo.Text = Me.ComboBox1.SelectedValue
            Else
                Clv_tipo.Text = 0
            End If
        End If
    End Sub

    Private Sub ComboBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.LostFocus
        If ComboBox3.SelectedIndex = -1 Then
            ComboBox2.ResetText()
        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Me.ComboBox2.SelectedIndex = Me.ComboBox3.SelectedIndex
    End Sub

    Private Sub ComboBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.LostFocus
        If ComboBox2.SelectedIndex = -1 Then
            ComboBox3.ResetText()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ComboBox3.SelectedIndex = Me.ComboBox2.SelectedIndex

    End Sub


End Class