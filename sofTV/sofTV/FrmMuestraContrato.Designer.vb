<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMuestraContrato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.MuestraNombreContratoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraNombreContratoTableAdapter = New sofTV.DataSetEric2TableAdapters.MuestraNombreContratoTableAdapter()
        Me.MuestraNombreContratoDataGridView = New System.Windows.Forms.DataGridView()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Domicilio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Colonia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clvElector = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clvAgua = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clvLuz = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraNombreContratoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraNombreContratoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel2.Location = New System.Drawing.Point(424, 353)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(281, 24)
        Me.CMBLabel2.TabIndex = 22
        Me.CMBLabel2.Text = "¿Desea Agregar El Contrato?"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(375, 392)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(177, 40)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&GUARDAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Maroon
        Me.CMBLabel1.Location = New System.Drawing.Point(25, 54)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(274, 20)
        Me.CMBLabel1.TabIndex = 20
        Me.CMBLabel1.Text = "Contratos Con El Mismo Nombre:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Olive
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(587, 392)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(165, 40)
        Me.Button2.TabIndex = 19
        Me.Button2.Text = "&RECHAZAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraNombreContratoBindingSource
        '
        Me.MuestraNombreContratoBindingSource.DataMember = "MuestraNombreContrato"
        Me.MuestraNombreContratoBindingSource.DataSource = Me.DataSetEric2
        '
        'MuestraNombreContratoTableAdapter
        '
        Me.MuestraNombreContratoTableAdapter.ClearBeforeFill = True
        '
        'MuestraNombreContratoDataGridView
        '
        Me.MuestraNombreContratoDataGridView.AllowUserToAddRows = False
        Me.MuestraNombreContratoDataGridView.AllowUserToDeleteRows = False
        Me.MuestraNombreContratoDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraNombreContratoDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MuestraNombreContratoDataGridView.ColumnHeadersHeight = 40
        Me.MuestraNombreContratoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Contrato, Me.Nombre, Me.Domicilio, Me.Colonia, Me.clvElector, Me.clvAgua, Me.clvLuz})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraNombreContratoDataGridView.DefaultCellStyle = DataGridViewCellStyle2
        Me.MuestraNombreContratoDataGridView.Location = New System.Drawing.Point(29, 77)
        Me.MuestraNombreContratoDataGridView.Name = "MuestraNombreContratoDataGridView"
        Me.MuestraNombreContratoDataGridView.ReadOnly = True
        Me.MuestraNombreContratoDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraNombreContratoDataGridView.Size = New System.Drawing.Size(995, 262)
        Me.MuestraNombreContratoDataGridView.TabIndex = 24
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'Domicilio
        '
        Me.Domicilio.DataPropertyName = "Domicilio"
        Me.Domicilio.HeaderText = "Domicilio"
        Me.Domicilio.Name = "Domicilio"
        Me.Domicilio.ReadOnly = True
        Me.Domicilio.Width = 200
        '
        'Colonia
        '
        Me.Colonia.DataPropertyName = "Colonia"
        Me.Colonia.HeaderText = "Colonia"
        Me.Colonia.Name = "Colonia"
        Me.Colonia.ReadOnly = True
        Me.Colonia.Width = 150
        '
        'clvElector
        '
        Me.clvElector.DataPropertyName = "clvElector"
        Me.clvElector.HeaderText = "Clave de Elector"
        Me.clvElector.Name = "clvElector"
        Me.clvElector.ReadOnly = True
        '
        'clvAgua
        '
        Me.clvAgua.DataPropertyName = "clvAgua"
        Me.clvAgua.HeaderText = "Recibo de Agua"
        Me.clvAgua.Name = "clvAgua"
        Me.clvAgua.ReadOnly = True
        '
        'clvLuz
        '
        Me.clvLuz.DataPropertyName = "clvLuz"
        Me.clvLuz.HeaderText = "Recibo de Electricidad"
        Me.clvLuz.Name = "clvLuz"
        Me.clvLuz.ReadOnly = True
        '
        'FrmMuestraContrato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1065, 452)
        Me.Controls.Add(Me.MuestraNombreContratoDataGridView)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button2)
        Me.Name = "FrmMuestraContrato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contratos Con Nombre Igual"
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraNombreContratoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraNombreContratoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents MuestraNombreContratoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraNombreContratoTableAdapter As sofTV.DataSetEric2TableAdapters.MuestraNombreContratoTableAdapter
    Friend WithEvents MuestraNombreContratoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Domicilio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Colonia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clvElector As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clvAgua As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clvLuz As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
