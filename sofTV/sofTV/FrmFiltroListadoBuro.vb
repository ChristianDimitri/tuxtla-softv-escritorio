﻿Imports System.IO
Imports System.Xml

Public Class FrmFiltroListadoBuro

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Public Function GeneraXml() As String
        Dim sw As StringWriter = New StringWriter()
        Dim w As XmlTextWriter = New XmlTextWriter(sw)
        w.Formatting = Formatting.Indented
        w.WriteStartElement("root")
        w.WriteStartElement("Opciones")
        w.WriteAttributeString("AunReportados", chkAunReportados.Checked)
        w.WriteAttributeString("FechaI", CDate(Me.dtpFechaI.Value.ToShortDateString()))
        w.WriteAttributeString("FechaF", CDate(Me.dtpFechaF.Value.ToShortDateString()))
        w.WriteEndElement()
        w.WriteEndElement()
        w.Close()
        GeneraXml = sw.ToString()
    End Function
End Class