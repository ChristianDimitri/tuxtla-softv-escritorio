<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRefBancaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Label3 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRefBancaria))
        Me.Button5 = New System.Windows.Forms.Button
        Me.Consulta_PaisesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Muestra_Bancos1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetyahve = New sofTV.DataSetyahve
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Muestra_Bancos1TableAdapter = New sofTV.DataSetyahveTableAdapters.Muestra_Bancos1TableAdapter
        Me.Muestra_Nombre_ClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Nombre_ClienteTableAdapter = New sofTV.DataSetyahveTableAdapters.Muestra_Nombre_ClienteTableAdapter
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Label3 = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        Label2 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        CType(Me.Consulta_PaisesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_PaisesBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.Muestra_Bancos1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Nombre_ClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(18, 110)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(85, 15)
        Label3.TabIndex = 4
        Label3.Text = "Referencia :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(34, 48)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(69, 15)
        Label1.TabIndex = 0
        Label1.Text = "Contrato :"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(48, 78)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(55, 15)
        Label2.TabIndex = 2
        Label2.Text = "Banco :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(37, 19)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(66, 15)
        Label4.TabIndex = 7
        Label4.Text = "Nombre :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(348, 207)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 20
        Me.Button5.Text = "SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Consulta_PaisesBindingNavigator
        '
        Me.Consulta_PaisesBindingNavigator.AddNewItem = Nothing
        Me.Consulta_PaisesBindingNavigator.CountItem = Nothing
        Me.Consulta_PaisesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_PaisesBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_PaisesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.BindingNavigatorSaveItem})
        Me.Consulta_PaisesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_PaisesBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_PaisesBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_PaisesBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_PaisesBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_PaisesBindingNavigator.Name = "Consulta_PaisesBindingNavigator"
        Me.Consulta_PaisesBindingNavigator.PositionItem = Nothing
        Me.Consulta_PaisesBindingNavigator.Size = New System.Drawing.Size(500, 25)
        Me.Consulta_PaisesBindingNavigator.TabIndex = 22
        Me.Consulta_PaisesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Enabled = False
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(74, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'BindingNavigatorSaveItem
        '
        Me.BindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorSaveItem.Enabled = False
        Me.BindingNavigatorSaveItem.Image = CType(resources.GetObject("BindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorSaveItem.Name = "BindingNavigatorSaveItem"
        Me.BindingNavigatorSaveItem.Size = New System.Drawing.Size(114, 22)
        Me.BindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.ComboBox2)
        Me.Panel1.Controls.Add(Label4)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Label3)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 40)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(472, 151)
        Me.Panel1.TabIndex = 23
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.Muestra_Nombre_ClienteBindingSource
        Me.ComboBox2.DisplayMember = "NOMBRE"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(109, 19)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(341, 21)
        Me.ComboBox2.TabIndex = 8
        Me.ComboBox2.ValueMember = "CONTRATO"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.Muestra_Bancos1BindingSource
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(109, 77)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(341, 21)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.ValueMember = "clave"
        '
        'Muestra_Bancos1BindingSource
        '
        Me.Muestra_Bancos1BindingSource.DataMember = "Muestra_Bancos1"
        Me.Muestra_Bancos1BindingSource.DataSource = Me.DataSetyahve
        '
        'DataSetyahve
        '
        Me.DataSetyahve.DataSetName = "DataSetyahve"
        Me.DataSetyahve.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(109, 108)
        Me.TextBox2.MaxLength = 50
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(183, 21)
        Me.TextBox2.TabIndex = 5
        '
        'Muestra_Bancos1TableAdapter
        '
        Me.Muestra_Bancos1TableAdapter.ClearBeforeFill = True
        '
        'Muestra_Nombre_ClienteBindingSource
        '
        Me.Muestra_Nombre_ClienteBindingSource.DataMember = "Muestra_Nombre_Cliente"
        Me.Muestra_Nombre_ClienteBindingSource.DataSource = Me.DataSetyahve
        '
        'Muestra_Nombre_ClienteTableAdapter
        '
        Me.Muestra_Nombre_ClienteTableAdapter.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(109, 48)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 21)
        Me.TextBox1.TabIndex = 3
        '
        'FrmRefBancaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 258)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Consulta_PaisesBindingNavigator)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "FrmRefBancaria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Referencia Bancaria"
        CType(Me.Consulta_PaisesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_PaisesBindingNavigator.ResumeLayout(False)
        Me.Consulta_PaisesBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Muestra_Bancos1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetyahve, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Nombre_ClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Consulta_PaisesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetyahve As sofTV.DataSetyahve
    Friend WithEvents Muestra_Bancos1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Bancos1TableAdapter As sofTV.DataSetyahveTableAdapters.Muestra_Bancos1TableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_Nombre_ClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Nombre_ClienteTableAdapter As sofTV.DataSetyahveTableAdapters.Muestra_Nombre_ClienteTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class
