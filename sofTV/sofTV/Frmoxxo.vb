Imports System.Data.SqlClient

Public Class Frmoxxo

    Private Prefijo As String = Nothing
    Private Sub DameDatosBitacora()
        Try
            Prefijo = Me.Prefijo_ProveedorTextBox.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuardaDatosBitacora()
        Try
            bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Prefijo del Proveedor", Prefijo, Me.Prefijo_ProveedorTextBox.Text, LocClv_Ciudad)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Frmoxxo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.CONGeneralesOxxo' Puede moverla o quitarla seg�n sea necesario.
        Me.CONGeneralesOxxoTableAdapter.Connection = CON
        Me.CONGeneralesOxxoTableAdapter.Fill(Me.DataSetEDGAR.CONGeneralesOxxo)
        CON.Close()
        DameDatosBitacora()

    End Sub

    Private Sub guardar()
        Try
            If Len(Trim(Me.Prefijo_ProveedorTextBox.Text)) > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.GUARDAGeneralesOxxoTableAdapter.Connection = CON
                Me.GUARDAGeneralesOxxoTableAdapter.Fill(Me.DataSetEDGAR.GUARDAGeneralesOxxo, Me.Prefijo_ProveedorTextBox.Text)
                CON.Close()
                GuardaDatosBitacora()
                MsgBox(mensaje5)
                Me.Close()
            Else
                MsgBox("Capture los Datos Requeridos ", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        guardar()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class