﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepDeco
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbCont = New System.Windows.Forms.CheckBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.cbInst = New System.Windows.Forms.CheckBox()
        Me.cbDesco = New System.Windows.Forms.CheckBox()
        Me.cbSusp = New System.Windows.Forms.CheckBox()
        Me.cbBaja = New System.Windows.Forms.CheckBox()
        Me.cbFuera = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbCont
        '
        Me.cbCont.AutoSize = True
        Me.cbCont.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCont.ForeColor = System.Drawing.Color.Black
        Me.cbCont.Location = New System.Drawing.Point(30, 32)
        Me.cbCont.Name = "cbCont"
        Me.cbCont.Size = New System.Drawing.Size(96, 19)
        Me.cbCont.TabIndex = 0
        Me.cbCont.Text = "Contratado"
        Me.cbCont.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(22, 154)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'cbInst
        '
        Me.cbInst.AutoSize = True
        Me.cbInst.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbInst.ForeColor = System.Drawing.Color.Black
        Me.cbInst.Location = New System.Drawing.Point(185, 32)
        Me.cbInst.Name = "cbInst"
        Me.cbInst.Size = New System.Drawing.Size(85, 19)
        Me.cbInst.TabIndex = 2
        Me.cbInst.Text = "Instalado"
        Me.cbInst.UseVisualStyleBackColor = True
        '
        'cbDesco
        '
        Me.cbDesco.AutoSize = True
        Me.cbDesco.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDesco.ForeColor = System.Drawing.Color.Black
        Me.cbDesco.Location = New System.Drawing.Point(30, 57)
        Me.cbDesco.Name = "cbDesco"
        Me.cbDesco.Size = New System.Drawing.Size(117, 19)
        Me.cbDesco.TabIndex = 3
        Me.cbDesco.Text = "Desconectado"
        Me.cbDesco.UseVisualStyleBackColor = True
        '
        'cbSusp
        '
        Me.cbSusp.AutoSize = True
        Me.cbSusp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbSusp.ForeColor = System.Drawing.Color.Black
        Me.cbSusp.Location = New System.Drawing.Point(185, 57)
        Me.cbSusp.Name = "cbSusp"
        Me.cbSusp.Size = New System.Drawing.Size(102, 19)
        Me.cbSusp.TabIndex = 4
        Me.cbSusp.Text = "Suspendido"
        Me.cbSusp.UseVisualStyleBackColor = True
        '
        'cbBaja
        '
        Me.cbBaja.AutoSize = True
        Me.cbBaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBaja.ForeColor = System.Drawing.Color.Black
        Me.cbBaja.Location = New System.Drawing.Point(30, 82)
        Me.cbBaja.Name = "cbBaja"
        Me.cbBaja.Size = New System.Drawing.Size(55, 19)
        Me.cbBaja.TabIndex = 5
        Me.cbBaja.Text = "Baja"
        Me.cbBaja.UseVisualStyleBackColor = True
        '
        'cbFuera
        '
        Me.cbFuera.AutoSize = True
        Me.cbFuera.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbFuera.ForeColor = System.Drawing.Color.Black
        Me.cbFuera.Location = New System.Drawing.Point(185, 82)
        Me.cbFuera.Name = "cbFuera"
        Me.cbFuera.Size = New System.Drawing.Size(116, 19)
        Me.cbFuera.TabIndex = 6
        Me.cbFuera.Text = "Fuera de Área"
        Me.cbFuera.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(187, 154)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "&CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbFuera)
        Me.GroupBox1.Controls.Add(Me.cbCont)
        Me.GroupBox1.Controls.Add(Me.cbInst)
        Me.GroupBox1.Controls.Add(Me.cbBaja)
        Me.GroupBox1.Controls.Add(Me.cbDesco)
        Me.GroupBox1.Controls.Add(Me.cbSusp)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(325, 118)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Status"
        '
        'FrmRepDeco
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(354, 208)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "FrmRepDeco"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Decodificadores"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbCont As System.Windows.Forms.CheckBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbInst As System.Windows.Forms.CheckBox
    Friend WithEvents cbDesco As System.Windows.Forms.CheckBox
    Friend WithEvents cbSusp As System.Windows.Forms.CheckBox
    Friend WithEvents cbBaja As System.Windows.Forms.CheckBox
    Friend WithEvents cbFuera As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
