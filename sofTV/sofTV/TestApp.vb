Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports Microsoft.VisualBasic
Namespace Org.Mentalis.Multimedia

    Public Class TestApp
        Public Shared Sub Main()
            Dim test As New TestApp()
            test.Start()
        End Sub
        Public Sub Start()
            WriteOptions()
            Dim input As String = Console.ReadLine()
            Dim si As String
            While Not input.ToLower().Equals("q")
                Console.WriteLine()
                Try
                    Select Case input.ToLower()
                        Case "m"
                            LoadMusicFile()
                        Case "v"
                            LoadVideoFile(Nothing)
                        Case "w"
                            LoadVideoOwnerFile()
                        Case "c"
                            mediaFile.Dispose()
                        Case "p"
                            mediaFile.Play()
                        Case "s"
                            mediaFile.StopPlay()
                        Case "a"
                            mediaFile.Paused = Not mediaFile.Paused
                        Case "l"
                            Console.WriteLine("Length=" + mediaFile.Length.ToString() + "ms.")
                        Case "d"
                            Console.Write("Enter the new playback speed (a number between 0 and 2267): ")
                            si = Console.ReadLine()
                            mediaFile.PlaybackSpeed = Integer.Parse(si)
                        Case "u"
                            Console.Write("Enter the new volume (a number between 0 and 1000): ")
                            si = Console.ReadLine()
                            mediaFile.Volume = Integer.Parse(si)
                        Case "o"
                            Console.WriteLine("Position=" + mediaFile.Position.ToString() + "ms.")
                        Case "n"
                            Console.WriteLine("Volume=" + mediaFile.Volume.ToString())
                        Case "t"
                            Console.WriteLine("Status=" + mediaFile.Status.ToString())
                        Case "r"
                            Console.WriteLine("Enter the new dimensions of the window (left, top, width, height), seperated by spaces\r\n eg:   0 0 640 480")
                            si = Console.ReadLine()
                            If si <> "" Then
                                Dim parts() As String = si.Split()
                                If parts.Length <> 4 Then Throw New Exception("User Error: Intelligence Resource Level Insufficient")
                                CType(mediaFile, VideoFile).OutputRect = New Rectangle(Integer.Parse(parts(0)), Integer.Parse(parts(1)), Integer.Parse(parts(2)), Integer.Parse(parts(3)))
                            End If
                        Case Else
                            Console.WriteLine("Command not understood :(")
                    End Select
                Catch e As Exception
                    Console.WriteLine("An error occured: " + e.Message)
                End Try
                WriteOptions()
                input = Console.ReadLine()
            End While
        End Sub
        Protected Sub WriteOptions()
            Console.Write(String.Format("{0} Multimedia Test Application - Options{0}" + _
                " -------------------------------------{0}{0}" + _
                "   m - Load a new music file{0}" + _
                "   v - Load a new video file{0}" + _
                "   w - Load a new video file in an owner window{0}" + _
                "   p - Play the media file{0}" + _
                "   s - Stop the media file{0}" + _
                "   a - Pause/unpause the media file{0}" + _
                "   d - Adjust the playback speed{0}" + _
                "   u - Adjust the volume of the media file{0}" + _
                "   r - Resize the video playback window{0}" + _
                "   l - Print the length of the media file{0}" + _
                "   o - Print the position of the media file{0}" + _
                "   t - Print the status of the media file{0}" + _
                "   n - Print the volume of the media file{0}" + _
                "   c - Close the loaded media file{0}" + _
                "   q - Quit the test application{0}{0}" + _
                "  Choose an option: " _
                , ControlChars.CrLf))
        End Sub
        Protected Sub LoadMusicFile()
            Console.WriteLine("Please enter the filename of the music file you want to load:" + ControlChars.CrLf + " (this can be any music type your system supports, for instance WAVE, MIDI, MP3, ...)")
            Dim file As String = Console.ReadLine()
            If Not file Is Nothing AndAlso file <> "" Then mediaFile = New SoundFile(file)
        End Sub
        Protected Sub LoadVideoOwnerFile()
            If Not parent Is Nothing Then parent.Dispose()
            parent = New MovieForm()
            parent.Text = "My VB.NET form!"
            parent.Show()
            LoadVideoFile(parent)
        End Sub
        Protected Sub LoadVideoFile(ByVal owner As IWin32Window)
            Console.WriteLine("Please enter the filename of the video file you want to load:\r\n (this can be any video type your system supports, for instance AVI, MPEG, DivX, ...)")
            Dim file As String = Console.ReadLine()
            If Not file Is Nothing AndAlso file <> "" Then mediaFile = New VideoFile(file, owner)
        End Sub
        Private parent As MovieForm
        Private mediaFile As mediaFile
    End Class

    Public Class MovieForm
        Inherits Form
        Private Sub InitializeComponent()
            Me.ClientSize = New System.Drawing.Size(640, 480)
            Me.Name = "MovieForm"
        End Sub
    End Class

End Namespace