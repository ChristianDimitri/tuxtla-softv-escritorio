<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGeneralesBancos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_UnicaLabel As System.Windows.Forms.Label
        Dim ClaveRelLabel As System.Windows.Forms.Label
        Dim SucursalLabel As System.Windows.Forms.Label
        Dim EmisoraLabel As System.Windows.Forms.Label
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGeneralesBancos))
        Me.DataSetEric = New sofTV.DataSetEric
        Me.ConGeneralesBancosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConGeneralesBancosTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralesBancosTableAdapter
        Me.Clv_UnicaTextBox = New System.Windows.Forms.TextBox
        Me.ClaveRelTextBox = New System.Windows.Forms.TextBox
        Me.SucursalTextBox = New System.Windows.Forms.TextBox
        Me.EmisoraTextBox = New System.Windows.Forms.TextBox
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ConGeneralesBancosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ConGeneralesBancosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel2 = New System.Windows.Forms.Panel
        Clv_UnicaLabel = New System.Windows.Forms.Label
        ClaveRelLabel = New System.Windows.Forms.Label
        SucursalLabel = New System.Windows.Forms.Label
        EmisoraLabel = New System.Windows.Forms.Label
        ConsecutivoLabel = New System.Windows.Forms.Label
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralesBancosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConGeneralesBancosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConGeneralesBancosBindingNavigator.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_UnicaLabel
        '
        Clv_UnicaLabel.AutoSize = True
        Clv_UnicaLabel.Location = New System.Drawing.Point(0, 1)
        Clv_UnicaLabel.Name = "Clv_UnicaLabel"
        Clv_UnicaLabel.Size = New System.Drawing.Size(56, 13)
        Clv_UnicaLabel.TabIndex = 2
        Clv_UnicaLabel.Text = "Clv Unica:"
        '
        'ClaveRelLabel
        '
        ClaveRelLabel.AutoSize = True
        ClaveRelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveRelLabel.Location = New System.Drawing.Point(26, 16)
        ClaveRelLabel.Name = "ClaveRelLabel"
        ClaveRelLabel.Size = New System.Drawing.Size(85, 18)
        ClaveRelLabel.TabIndex = 4
        ClaveRelLabel.Text = "Clave Rel:"
        '
        'SucursalLabel
        '
        SucursalLabel.AutoSize = True
        SucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SucursalLabel.Location = New System.Drawing.Point(32, 63)
        SucursalLabel.Name = "SucursalLabel"
        SucursalLabel.Size = New System.Drawing.Size(79, 18)
        SucursalLabel.TabIndex = 6
        SucursalLabel.Text = "Sucursal:"
        AddHandler SucursalLabel.Click, AddressOf Me.SucursalLabel_Click
        '
        'EmisoraLabel
        '
        EmisoraLabel.AutoSize = True
        EmisoraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmisoraLabel.Location = New System.Drawing.Point(35, 107)
        EmisoraLabel.Name = "EmisoraLabel"
        EmisoraLabel.Size = New System.Drawing.Size(76, 18)
        EmisoraLabel.TabIndex = 8
        EmisoraLabel.Text = "Emisora:"
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.Location = New System.Drawing.Point(4, 148)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(107, 18)
        ConsecutivoLabel.TabIndex = 10
        ConsecutivoLabel.Text = "Consecutivo:"
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConGeneralesBancosBindingSource
        '
        Me.ConGeneralesBancosBindingSource.DataMember = "ConGeneralesBancos"
        Me.ConGeneralesBancosBindingSource.DataSource = Me.DataSetEric
        '
        'ConGeneralesBancosTableAdapter
        '
        Me.ConGeneralesBancosTableAdapter.ClearBeforeFill = True
        '
        'Clv_UnicaTextBox
        '
        Me.Clv_UnicaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralesBancosBindingSource, "Clv_Unica", True))
        Me.Clv_UnicaTextBox.Location = New System.Drawing.Point(42, 17)
        Me.Clv_UnicaTextBox.Name = "Clv_UnicaTextBox"
        Me.Clv_UnicaTextBox.ReadOnly = True
        Me.Clv_UnicaTextBox.Size = New System.Drawing.Size(61, 20)
        Me.Clv_UnicaTextBox.TabIndex = 3
        Me.Clv_UnicaTextBox.TabStop = False
        '
        'ClaveRelTextBox
        '
        Me.ClaveRelTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralesBancosBindingSource, "ClaveRel", True))
        Me.ClaveRelTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveRelTextBox.Location = New System.Drawing.Point(117, 10)
        Me.ClaveRelTextBox.MaxLength = 2
        Me.ClaveRelTextBox.Name = "ClaveRelTextBox"
        Me.ClaveRelTextBox.ReadOnly = True
        Me.ClaveRelTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ClaveRelTextBox.TabIndex = 5
        Me.ClaveRelTextBox.TabStop = False
        '
        'SucursalTextBox
        '
        Me.SucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralesBancosBindingSource, "Sucursal", True))
        Me.SucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SucursalTextBox.Location = New System.Drawing.Point(117, 57)
        Me.SucursalTextBox.MaxLength = 10
        Me.SucursalTextBox.Name = "SucursalTextBox"
        Me.SucursalTextBox.Size = New System.Drawing.Size(283, 24)
        Me.SucursalTextBox.TabIndex = 0
        '
        'EmisoraTextBox
        '
        Me.EmisoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralesBancosBindingSource, "Emisora", True))
        Me.EmisoraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmisoraTextBox.Location = New System.Drawing.Point(117, 101)
        Me.EmisoraTextBox.MaxLength = 10
        Me.EmisoraTextBox.Name = "EmisoraTextBox"
        Me.EmisoraTextBox.Size = New System.Drawing.Size(283, 24)
        Me.EmisoraTextBox.TabIndex = 1
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralesBancosBindingSource, "Consecutivo", True))
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(117, 148)
        Me.ConsecutivoTextBox.MaxLength = 8
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(283, 24)
        Me.ConsecutivoTextBox.TabIndex = 2
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConGeneralesBancosBindingNavigatorSaveItem
        '
        Me.ConGeneralesBancosBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConGeneralesBancosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConGeneralesBancosBindingNavigatorSaveItem.Name = "ConGeneralesBancosBindingNavigatorSaveItem"
        Me.ConGeneralesBancosBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.ConGeneralesBancosBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ConGeneralesBancosBindingNavigator
        '
        Me.ConGeneralesBancosBindingNavigator.AddNewItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.BindingSource = Me.ConGeneralesBancosBindingSource
        Me.ConGeneralesBancosBindingNavigator.CountItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConGeneralesBancosBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConGeneralesBancosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConGeneralesBancosBindingNavigatorSaveItem})
        Me.ConGeneralesBancosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConGeneralesBancosBindingNavigator.MoveFirstItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.MoveLastItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.MoveNextItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.MovePreviousItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.Name = "ConGeneralesBancosBindingNavigator"
        Me.ConGeneralesBancosBindingNavigator.PositionItem = Nothing
        Me.ConGeneralesBancosBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConGeneralesBancosBindingNavigator.Size = New System.Drawing.Size(478, 25)
        Me.ConGeneralesBancosBindingNavigator.TabIndex = 3
        Me.ConGeneralesBancosBindingNavigator.TabStop = True
        Me.ConGeneralesBancosBindingNavigator.Text = "BindingNavigator1"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Clv_UnicaTextBox)
        Me.Panel1.Controls.Add(Clv_UnicaLabel)
        Me.Panel1.Location = New System.Drawing.Point(172, 156)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(10, 10)
        Me.Panel1.TabIndex = 12
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(330, 263)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(SucursalLabel)
        Me.Panel2.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel2.Controls.Add(ConsecutivoLabel)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.EmisoraTextBox)
        Me.Panel2.Controls.Add(ClaveRelLabel)
        Me.Panel2.Controls.Add(EmisoraLabel)
        Me.Panel2.Controls.Add(Me.ClaveRelTextBox)
        Me.Panel2.Controls.Add(Me.SucursalTextBox)
        Me.Panel2.Location = New System.Drawing.Point(12, 42)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(437, 215)
        Me.Panel2.TabIndex = 0
        Me.Panel2.TabStop = True
        '
        'FrmGeneralesBancos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 315)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConGeneralesBancosBindingNavigator)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "FrmGeneralesBancos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales Bancos"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralesBancosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConGeneralesBancosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConGeneralesBancosBindingNavigator.ResumeLayout(False)
        Me.ConGeneralesBancosBindingNavigator.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConGeneralesBancosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralesBancosTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralesBancosTableAdapter
    Friend WithEvents Clv_UnicaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveRelTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmisoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConGeneralesBancosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConGeneralesBancosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
End Class
