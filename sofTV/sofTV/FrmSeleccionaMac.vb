﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Public Class FrmSeleccionaMac

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        MacRuter = ComboBox1.SelectedValue
        AgregaRouterAlCliente(MacRuter, Contrato)
        SiAplica = 0
        Regresar = 0
        Me.Close()
    End Sub

    Private Sub AgregaRouterAlCliente(ByVal mac As Integer, ByVal contrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("AgregaRouterAlCliente", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Mac", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = mac
        CMD.Parameters.Add(PRM2)

       

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            ProcesoTerminado = 1
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub FrmSeleccionaMac_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("MUESMACDisponibles", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@OP", SqlDbType.Int).Value = 0
            Adaptador.SelectCommand.Parameters.Add("@CLV_TECNICO", SqlDbType.Int).Value = FrmOrdSer.Tecnico.SelectedValue
            Adaptador.SelectCommand.Parameters.Add("@CLV_ORDEN", SqlDbType.Int).Value = FrmOrdSer.Clv_OrdenTextBox.Text



            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "MUESMACDisponibles")
            Bs.DataSource = Dataset.Tables("MUESMACDisponibles")

            'dgvResultadosClasificacion.DataSource = Bs
            ComboBox1.DataSource = Bs
            ComboBox1.DisplayMember = "MACCABLEMODEM"
            ComboBox1.ValueMember = "Clv_Cablemodem"

            If ComboBox1.SelectedValue = 0 Then
                MessageBox.Show("El tecnico no tiene cablemodems disponibles.", "¡Atención!", MessageBoxButtons.OK)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try
    End Sub
    Private Sub FrmSeleccionaMac_closed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.FormClosed
        Regresar = 1
        Me.Close() 'Con esto se llamará a FormClosing 
    End Sub

End Class