﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetDecodificadores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cbDecodificador = New System.Windows.Forms.ComboBox()
        Me.cbTipoCliente = New System.Windows.Forms.ComboBox()
        Me.cbServicio = New System.Windows.Forms.ComboBox()
        Me.dgvDet = New System.Windows.Forms.DataGridView()
        Me.Detalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioPrincipal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioAdicional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel77 = New System.Windows.Forms.Label()
        Me.txtPrecioPrincipal = New System.Windows.Forms.TextBox()
        Me.CMBLabel76 = New System.Windows.Forms.Label()
        Me.txtPrecioAdicional = New System.Windows.Forms.TextBox()
        Me.txtEliminarDec = New System.Windows.Forms.Button()
        Me.txtAgregarDec = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbDecodificador
        '
        Me.cbDecodificador.DisplayMember = "Descripcion"
        Me.cbDecodificador.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDecodificador.FormattingEnabled = True
        Me.cbDecodificador.Location = New System.Drawing.Point(269, 18)
        Me.cbDecodificador.Name = "cbDecodificador"
        Me.cbDecodificador.Size = New System.Drawing.Size(308, 23)
        Me.cbDecodificador.TabIndex = 0
        Me.cbDecodificador.ValueMember = "Id"
        '
        'cbTipoCliente
        '
        Me.cbTipoCliente.DisplayMember = "Descripcion"
        Me.cbTipoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoCliente.FormattingEnabled = True
        Me.cbTipoCliente.Location = New System.Drawing.Point(269, 47)
        Me.cbTipoCliente.Name = "cbTipoCliente"
        Me.cbTipoCliente.Size = New System.Drawing.Size(308, 23)
        Me.cbTipoCliente.TabIndex = 2
        Me.cbTipoCliente.ValueMember = "Clv_TipoCliente"
        '
        'cbServicio
        '
        Me.cbServicio.DisplayMember = "Descripcion"
        Me.cbServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbServicio.FormattingEnabled = True
        Me.cbServicio.Location = New System.Drawing.Point(269, 92)
        Me.cbServicio.Name = "cbServicio"
        Me.cbServicio.Size = New System.Drawing.Size(308, 23)
        Me.cbServicio.TabIndex = 3
        Me.cbServicio.ValueMember = "Clv_Servicio"
        '
        'dgvDet
        '
        Me.dgvDet.AllowUserToAddRows = False
        Me.dgvDet.AllowUserToDeleteRows = False
        Me.dgvDet.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Detalle, Me.Servicio, Me.PrecioPrincipal, Me.PrecioAdicional})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDet.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDet.Location = New System.Drawing.Point(58, 204)
        Me.dgvDet.Name = "dgvDet"
        Me.dgvDet.ReadOnly = True
        Me.dgvDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDet.Size = New System.Drawing.Size(607, 281)
        Me.dgvDet.TabIndex = 4
        '
        'Detalle
        '
        Me.Detalle.DataPropertyName = "Detalle"
        Me.Detalle.HeaderText = "Detalle"
        Me.Detalle.Name = "Detalle"
        Me.Detalle.ReadOnly = True
        Me.Detalle.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        Me.Servicio.Width = 250
        '
        'PrecioPrincipal
        '
        Me.PrecioPrincipal.DataPropertyName = "PrecioPrincipal"
        Me.PrecioPrincipal.HeaderText = "PrecioPrincipal"
        Me.PrecioPrincipal.Name = "PrecioPrincipal"
        Me.PrecioPrincipal.ReadOnly = True
        Me.PrecioPrincipal.Width = 150
        '
        'PrecioAdicional
        '
        Me.PrecioAdicional.DataPropertyName = "PrecioAdicional"
        Me.PrecioAdicional.HeaderText = "PrecioAdicional"
        Me.PrecioAdicional.Name = "PrecioAdicional"
        Me.PrecioAdicional.ReadOnly = True
        Me.PrecioAdicional.Width = 150
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(163, 26)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(100, 15)
        Me.CMBLabel1.TabIndex = 5
        Me.CMBLabel1.Text = "Decodificador:"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(175, 55)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(88, 15)
        Me.CMBLabel3.TabIndex = 7
        Me.CMBLabel3.Text = "Tipo Cliente:"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(140, 100)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(123, 15)
        Me.CMBLabel4.TabIndex = 8
        Me.CMBLabel4.Text = "Servicio Principal:"
        '
        'CMBLabel77
        '
        Me.CMBLabel77.AutoSize = True
        Me.CMBLabel77.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel77.Location = New System.Drawing.Point(150, 127)
        Me.CMBLabel77.Name = "CMBLabel77"
        Me.CMBLabel77.Size = New System.Drawing.Size(113, 15)
        Me.CMBLabel77.TabIndex = 69
        Me.CMBLabel77.Text = "Precio Principal:"
        '
        'txtPrecioPrincipal
        '
        Me.txtPrecioPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioPrincipal.Location = New System.Drawing.Point(269, 121)
        Me.txtPrecioPrincipal.Name = "txtPrecioPrincipal"
        Me.txtPrecioPrincipal.Size = New System.Drawing.Size(160, 21)
        Me.txtPrecioPrincipal.TabIndex = 68
        '
        'CMBLabel76
        '
        Me.CMBLabel76.AutoSize = True
        Me.CMBLabel76.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel76.Location = New System.Drawing.Point(148, 154)
        Me.CMBLabel76.Name = "CMBLabel76"
        Me.CMBLabel76.Size = New System.Drawing.Size(115, 15)
        Me.CMBLabel76.TabIndex = 67
        Me.CMBLabel76.Text = "Precio Adicional:"
        '
        'txtPrecioAdicional
        '
        Me.txtPrecioAdicional.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioAdicional.Location = New System.Drawing.Point(269, 148)
        Me.txtPrecioAdicional.Name = "txtPrecioAdicional"
        Me.txtPrecioAdicional.Size = New System.Drawing.Size(160, 21)
        Me.txtPrecioAdicional.TabIndex = 66
        '
        'txtEliminarDec
        '
        Me.txtEliminarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEliminarDec.Location = New System.Drawing.Point(671, 204)
        Me.txtEliminarDec.Name = "txtEliminarDec"
        Me.txtEliminarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtEliminarDec.TabIndex = 72
        Me.txtEliminarDec.Text = "&Eliminar"
        Me.txtEliminarDec.UseVisualStyleBackColor = True
        '
        'txtAgregarDec
        '
        Me.txtAgregarDec.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgregarDec.Location = New System.Drawing.Point(671, 146)
        Me.txtAgregarDec.Name = "txtAgregarDec"
        Me.txtAgregarDec.Size = New System.Drawing.Size(75, 23)
        Me.txtAgregarDec.TabIndex = 71
        Me.txtAgregarDec.Text = "&Agregar"
        Me.txtAgregarDec.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(610, 511)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 70
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'FrmDetDecodificadores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(758, 559)
        Me.Controls.Add(Me.txtEliminarDec)
        Me.Controls.Add(Me.txtAgregarDec)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.CMBLabel77)
        Me.Controls.Add(Me.txtPrecioPrincipal)
        Me.Controls.Add(Me.CMBLabel76)
        Me.Controls.Add(Me.txtPrecioAdicional)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.dgvDet)
        Me.Controls.Add(Me.cbServicio)
        Me.Controls.Add(Me.cbTipoCliente)
        Me.Controls.Add(Me.cbDecodificador)
        Me.Name = "FrmDetDecodificadores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Precio Decodificadores"
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbDecodificador As System.Windows.Forms.ComboBox
    Friend WithEvents cbTipoCliente As System.Windows.Forms.ComboBox
    Friend WithEvents cbServicio As System.Windows.Forms.ComboBox
    Friend WithEvents dgvDet As System.Windows.Forms.DataGridView
    Friend WithEvents Detalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioPrincipal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioAdicional As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel77 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel76 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioAdicional As System.Windows.Forms.TextBox
    Friend WithEvents txtEliminarDec As System.Windows.Forms.Button
    Friend WithEvents txtAgregarDec As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
