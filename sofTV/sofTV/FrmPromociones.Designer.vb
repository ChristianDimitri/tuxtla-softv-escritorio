﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPromociones))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.bSalir = New System.Windows.Forms.Button()
        Me.cServicio = New System.Windows.Forms.ComboBox()
        Me.tbNombre = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.dtFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.tbPrecio = New System.Windows.Forms.TextBox()
        Me.bnPromocion = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.TSBEliminar = New System.Windows.Forms.ToolStripButton()
        Me.TSBGuardar = New System.Windows.Forms.ToolStripButton()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbPagos = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.cbConcepto = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.checkPregunta = New System.Windows.Forms.CheckBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cbTipoCobro = New System.Windows.Forms.ComboBox()
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPromocion.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(90, 101)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 15)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Nombre :"
        '
        'bSalir
        '
        Me.bSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bSalir.Location = New System.Drawing.Point(469, 391)
        Me.bSalir.Name = "bSalir"
        Me.bSalir.Size = New System.Drawing.Size(136, 36)
        Me.bSalir.TabIndex = 1
        Me.bSalir.Text = "&SALIR"
        Me.bSalir.UseVisualStyleBackColor = True
        '
        'cServicio
        '
        Me.cServicio.DisplayMember = "Descripcion"
        Me.cServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cServicio.FormattingEnabled = True
        Me.cServicio.Location = New System.Drawing.Point(162, 125)
        Me.cServicio.Name = "cServicio"
        Me.cServicio.Size = New System.Drawing.Size(321, 23)
        Me.cServicio.TabIndex = 3
        Me.cServicio.ValueMember = "Clv_Servicio"
        '
        'tbNombre
        '
        Me.tbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombre.Location = New System.Drawing.Point(162, 98)
        Me.tbNombre.Name = "tbNombre"
        Me.tbNombre.Size = New System.Drawing.Size(321, 21)
        Me.tbNombre.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(90, 133)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 15)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Servicio :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(100, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 15)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Precio :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(59, 297)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(97, 15)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Fecha Inicial :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(66, 324)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 15)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Fecha Final :"
        '
        'dtFechaIni
        '
        Me.dtFechaIni.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaIni.Location = New System.Drawing.Point(162, 291)
        Me.dtFechaIni.Name = "dtFechaIni"
        Me.dtFechaIni.Size = New System.Drawing.Size(113, 21)
        Me.dtFechaIni.TabIndex = 9
        '
        'dtFechaFin
        '
        Me.dtFechaFin.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaFin.Location = New System.Drawing.Point(162, 318)
        Me.dtFechaFin.Name = "dtFechaFin"
        Me.dtFechaFin.Size = New System.Drawing.Size(113, 21)
        Me.dtFechaFin.TabIndex = 10
        '
        'tbPrecio
        '
        Me.tbPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPrecio.Location = New System.Drawing.Point(162, 154)
        Me.tbPrecio.Name = "tbPrecio"
        Me.tbPrecio.Size = New System.Drawing.Size(100, 21)
        Me.tbPrecio.TabIndex = 11
        Me.tbPrecio.Text = "0"
        '
        'bnPromocion
        '
        Me.bnPromocion.AddNewItem = Nothing
        Me.bnPromocion.CountItem = Nothing
        Me.bnPromocion.DeleteItem = Nothing
        Me.bnPromocion.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPromocion.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSBEliminar, Me.TSBGuardar})
        Me.bnPromocion.Location = New System.Drawing.Point(0, 0)
        Me.bnPromocion.MoveFirstItem = Nothing
        Me.bnPromocion.MoveLastItem = Nothing
        Me.bnPromocion.MoveNextItem = Nothing
        Me.bnPromocion.MovePreviousItem = Nothing
        Me.bnPromocion.Name = "bnPromocion"
        Me.bnPromocion.PositionItem = Nothing
        Me.bnPromocion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPromocion.Size = New System.Drawing.Size(617, 25)
        Me.bnPromocion.TabIndex = 13
        Me.bnPromocion.Text = "BindingNavigator1"
        '
        'TSBEliminar
        '
        Me.TSBEliminar.Image = CType(resources.GetObject("TSBEliminar.Image"), System.Drawing.Image)
        Me.TSBEliminar.Name = "TSBEliminar"
        Me.TSBEliminar.RightToLeftAutoMirrorImage = True
        Me.TSBEliminar.Size = New System.Drawing.Size(88, 22)
        Me.TSBEliminar.Text = "&ELIMINAR"
        '
        'TSBGuardar
        '
        Me.TSBGuardar.Image = CType(resources.GetObject("TSBGuardar.Image"), System.Drawing.Image)
        Me.TSBGuardar.Name = "TSBGuardar"
        Me.TSBGuardar.Size = New System.Drawing.Size(88, 22)
        Me.TSBGuardar.Text = "&GUARDAR"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label11.Location = New System.Drawing.Point(159, 261)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(158, 15)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Fechas de Contratación"
        '
        'tbPagos
        '
        Me.tbPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPagos.Location = New System.Drawing.Point(162, 181)
        Me.tbPagos.Name = "tbPagos"
        Me.tbPagos.Size = New System.Drawing.Size(100, 21)
        Me.tbPagos.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(55, 187)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 15)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "No. de Pagos :"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(162, 359)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(147, 19)
        Me.CheckBox1.TabIndex = 17
        Me.CheckBox1.Text = "Promoción Vigente"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'cbConcepto
        '
        Me.cbConcepto.DisplayMember = "Concepto"
        Me.cbConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbConcepto.FormattingEnabled = True
        Me.cbConcepto.Location = New System.Drawing.Point(162, 69)
        Me.cbConcepto.Name = "cbConcepto"
        Me.cbConcepto.Size = New System.Drawing.Size(199, 23)
        Me.cbConcepto.TabIndex = 18
        Me.cbConcepto.ValueMember = "Clave"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(81, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 15)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Concepto :"
        '
        'checkPregunta
        '
        Me.checkPregunta.AutoSize = True
        Me.checkPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkPregunta.Location = New System.Drawing.Point(162, 219)
        Me.checkPregunta.Name = "checkPregunta"
        Me.checkPregunta.Size = New System.Drawing.Size(386, 19)
        Me.checkPregunta.TabIndex = 21
        Me.checkPregunta.Text = "¿Se preguntará si desea hacer el cobro en Facturación?"
        Me.checkPregunta.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(51, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(105, 15)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Tipo de Cobro :"
        '
        'cbTipoCobro
        '
        Me.cbTipoCobro.DisplayMember = "Descripcion"
        Me.cbTipoCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoCobro.FormattingEnabled = True
        Me.cbTipoCobro.Location = New System.Drawing.Point(162, 40)
        Me.cbTipoCobro.Name = "cbTipoCobro"
        Me.cbTipoCobro.Size = New System.Drawing.Size(199, 23)
        Me.cbTipoCobro.TabIndex = 24
        Me.cbTipoCobro.ValueMember = "Clv_TipoCliente"
        '
        'FrmPromociones
        '
        Me.ClientSize = New System.Drawing.Size(617, 439)
        Me.Controls.Add(Me.cbTipoCobro)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.checkPregunta)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.cbConcepto)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.tbPagos)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.bnPromocion)
        Me.Controls.Add(Me.tbPrecio)
        Me.Controls.Add(Me.dtFechaFin)
        Me.Controls.Add(Me.dtFechaIni)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tbNombre)
        Me.Controls.Add(Me.cServicio)
        Me.Controls.Add(Me.bSalir)
        Me.Controls.Add(Me.Label6)
        Me.Name = "FrmPromociones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promoción"
        CType(Me.bnPromocion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPromocion.ResumeLayout(False)
        Me.bnPromocion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents cbServicio As System.Windows.Forms.ComboBox
    Friend WithEvents cbVigente As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents bSalir As System.Windows.Forms.Button
    Friend WithEvents cServicio As System.Windows.Forms.ComboBox
    Friend WithEvents tbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbPrecio As System.Windows.Forms.TextBox
    Friend WithEvents bnPromocion As System.Windows.Forms.BindingNavigator
    Friend WithEvents TSBEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSBGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbPagos As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents cbConcepto As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents checkPregunta As System.Windows.Forms.CheckBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cbTipoCobro As System.Windows.Forms.ComboBox
End Class
