﻿Public Class BrwEstadoCuenta

    Private Sub BrwEstadoCuenta_Load(sender As Object, e As EventArgs) Handles Me.Load
        llenaEstadoCuenta(0)
        If Me.ResumenClienteDataGridView.RowCount > 0 Then
            GloIdEstadoCuentaR = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
        End If
    End Sub

    Private Sub llenaEstadoCuenta(ByVal Op As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Fecha", SqlDbType.Date, DateTimePicker1.Value)

        ResumenClienteDataGridView.DataSource = BaseII.ConsultaDT("Usp_ConsultaEstadoCuentaPeriodo")

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        llenaEstadoCuenta(1)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        eOpPPE = 9
        FrmImprimirPPE.Show()
    End Sub

    Private Sub ResumenClienteDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellClick
        If Me.ResumenClienteDataGridView.RowCount > 0 Then
            GloIdEstadoCuentaR = Me.ResumenClienteDataGridView.SelectedCells(0).Value.ToString
        End If
    End Sub

    Private Sub ResumenClienteDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ResumenClienteDataGridView.CellContentClick

    End Sub

    
End Class