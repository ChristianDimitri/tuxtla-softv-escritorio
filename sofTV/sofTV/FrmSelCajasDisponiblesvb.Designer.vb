﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelCajasDisponiblesvb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSelCajasDisponiblesvb))
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CajasComboBox = New System.Windows.Forms.ComboBox()
        Me.AceptarButton = New System.Windows.Forms.Button()
        Me.CerrarButton = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.ContNetCombo = New System.Windows.Forms.ComboBox()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.CajasToolStrip = New System.Windows.Forms.ToolStrip()
        Me.DeleteToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CajasToolStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Blue
        Me.CMBLabel1.Location = New System.Drawing.Point(6, 105)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(192, 15)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Cajas Digitales Disponibles :"
        '
        'CajasComboBox
        '
        Me.CajasComboBox.DisplayMember = "MACCABLEMODEM"
        Me.CajasComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CajasComboBox.FormattingEnabled = True
        Me.CajasComboBox.Location = New System.Drawing.Point(9, 124)
        Me.CajasComboBox.Name = "CajasComboBox"
        Me.CajasComboBox.Size = New System.Drawing.Size(360, 23)
        Me.CajasComboBox.TabIndex = 1
        Me.CajasComboBox.ValueMember = "Clv_Cablemodem"
        '
        'AceptarButton
        '
        Me.AceptarButton.BackColor = System.Drawing.Color.DarkOrange
        Me.AceptarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AceptarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AceptarButton.ForeColor = System.Drawing.Color.Black
        Me.AceptarButton.Location = New System.Drawing.Point(99, 177)
        Me.AceptarButton.Name = "AceptarButton"
        Me.AceptarButton.Size = New System.Drawing.Size(136, 33)
        Me.AceptarButton.TabIndex = 3
        Me.AceptarButton.Text = "&ACEPTAR"
        Me.AceptarButton.UseVisualStyleBackColor = False
        '
        'CerrarButton
        '
        Me.CerrarButton.BackColor = System.Drawing.Color.DarkOrange
        Me.CerrarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CerrarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CerrarButton.ForeColor = System.Drawing.Color.Black
        Me.CerrarButton.Location = New System.Drawing.Point(241, 177)
        Me.CerrarButton.Name = "CerrarButton"
        Me.CerrarButton.Size = New System.Drawing.Size(136, 33)
        Me.CerrarButton.TabIndex = 4
        Me.CerrarButton.Text = "&CERRAR"
        Me.CerrarButton.UseVisualStyleBackColor = False
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Blue
        Me.CMBLabel2.Location = New System.Drawing.Point(6, 52)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(179, 15)
        Me.CMBLabel2.TabIndex = 5
        Me.CMBLabel2.Text = "Contrato Digital a Asingar :"
        '
        'ContNetCombo
        '
        Me.ContNetCombo.DisplayMember = "Descripcion"
        Me.ContNetCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContNetCombo.FormattingEnabled = True
        Me.ContNetCombo.Location = New System.Drawing.Point(9, 70)
        Me.ContNetCombo.Name = "ContNetCombo"
        Me.ContNetCombo.Size = New System.Drawing.Size(360, 23)
        Me.ContNetCombo.TabIndex = 6
        Me.ContNetCombo.ValueMember = "ContratoNet"
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.BackColor = System.Drawing.Color.Yellow
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.Red
        Me.CMBLabel3.Location = New System.Drawing.Point(14, 150)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(161, 15)
        Me.CMBLabel3.TabIndex = 42
        Me.CMBLabel3.Text = "¡Ya tiene caja asignada!"
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'CajasToolStrip
        '
        Me.CajasToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.CajasToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripButton})
        Me.CajasToolStrip.Location = New System.Drawing.Point(217, 31)
        Me.CajasToolStrip.Name = "CajasToolStrip"
        Me.CajasToolStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CajasToolStrip.Size = New System.Drawing.Size(152, 25)
        Me.CajasToolStrip.TabIndex = 43
        Me.CajasToolStrip.Text = "ToolStrip1"
        '
        'DeleteToolStripButton
        '
        Me.DeleteToolStripButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteToolStripButton.Image = CType(resources.GetObject("DeleteToolStripButton.Image"), System.Drawing.Image)
        Me.DeleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteToolStripButton.Name = "DeleteToolStripButton"
        Me.DeleteToolStripButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.DeleteToolStripButton.Size = New System.Drawing.Size(109, 22)
        Me.DeleteToolStripButton.Text = "&Eliminar Caja"
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CajasToolStrip)
        Me.Panel1.Controls.Add(Me.CMBLabel3)
        Me.Panel1.Controls.Add(Me.CMBLabel2)
        Me.Panel1.Controls.Add(Me.ContNetCombo)
        Me.Panel1.Controls.Add(Me.CMBLabel1)
        Me.Panel1.Controls.Add(Me.CajasComboBox)
        Me.Panel1.Controls.Add(Me.AceptarButton)
        Me.Panel1.Controls.Add(Me.CerrarButton)
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(382, 217)
        Me.Panel1.TabIndex = 44
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(-10, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(400, 31)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Asignación de Cajas Digitales"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmSelCajasDisponiblesvb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 219)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSelCajasDisponiblesvb"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cajas Digitales Disponibles"
        Me.CajasToolStrip.ResumeLayout(False)
        Me.CajasToolStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CajasComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents AceptarButton As System.Windows.Forms.Button
    Friend WithEvents CerrarButton As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents ContNetCombo As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents CajasToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents DeleteToolStripButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
