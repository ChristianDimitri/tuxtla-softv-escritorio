﻿Public Class FrmAparatosAdicionales

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmAparatosAdicionales_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        DameAparatosAdicionales()
    End Sub

    Private Sub DameAparatosAdicionales()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contratonet", SqlDbType.BigInt, GloContratonet)
        DataGridView1.DataSource = BaseII.ConsultaDT("DameAparatosAdicionales")
    End Sub
End Class