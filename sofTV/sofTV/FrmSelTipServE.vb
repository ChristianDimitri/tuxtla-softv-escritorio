Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelTipServE


    Private Sub FrmSelTipServE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'colorea(Me, Me.Name)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.MuestraTipServEricTableAdapter.Connection = CON
        'Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
        'CON.Close()
        Me.MuestraTipSer(0, 0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If rquitaServ = True Then
            Dim quitaSer As FrmQuitaServicios = New FrmQuitaServicios
            quitaSer.llenaGrid(Me.ConceptoComboBox.SelectedValue)
            quitaSer.Show()
            rquitaServ = False
            Me.Close()
            Return
        End If
        If Me.ConceptoComboBox.SelectedValue > 0 Then
            eTipSer = Me.ConceptoComboBox.SelectedValue
            eServicio = Me.ConceptoComboBox.Text
            'If eBndContratoF = True Then
            '    eBndContratoF = False
            '    FrmImprimirComision.Show()
            'Else
            If eOpVentas = 61 And eOpPPE = 0 Then
                FrmImprimirComision.Show()
            Else
                FrmSelServicioE.Show()
            End If

            'End If
            Me.Close()
        End If
    End Sub

    Private Sub MuestraTipSer(ByVal Clv_TipSer As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipServEric ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As DataTable = New DataTable
        Dim bindingSource As BindingSource = New BindingSource
        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ConceptoComboBox.DataSource = bindingSource
            ConceptoComboBox.DisplayMember = "Concepto"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged

    End Sub
End Class