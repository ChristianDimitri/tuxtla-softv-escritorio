﻿Public Class FrmSelStatus

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        LocBndI = False
        LocBndB = False
        LocBndD = False
        LocBndS = False
        If cbxActivos.Checked = True Then
            LocBndI = True
        End If
        If cbxCancelado.Checked = True Then
            LocBndB = True
        End If
        If cbxSuspendido.Checked = True Then
            LocBndD = True
        End If
        If cbxSupenciónTemp.Checked = True Then
            LocBndS = True
        End If
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub FrmSelStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub
End Class