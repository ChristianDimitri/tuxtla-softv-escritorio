﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelStatus
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBpnlStatus = New System.Windows.Forms.Panel()
        Me.cbxSupenciónTemp = New System.Windows.Forms.CheckBox()
        Me.cbxCancelado = New System.Windows.Forms.CheckBox()
        Me.cbxSuspendido = New System.Windows.Forms.CheckBox()
        Me.cbxActivos = New System.Windows.Forms.CheckBox()
        Me.CMBlblTitulo = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.CMBpnlStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBpnlStatus
        '
        Me.CMBpnlStatus.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBpnlStatus.Controls.Add(Me.cbxSupenciónTemp)
        Me.CMBpnlStatus.Controls.Add(Me.cbxCancelado)
        Me.CMBpnlStatus.Controls.Add(Me.cbxSuspendido)
        Me.CMBpnlStatus.Controls.Add(Me.cbxActivos)
        Me.CMBpnlStatus.Location = New System.Drawing.Point(22, 45)
        Me.CMBpnlStatus.Name = "CMBpnlStatus"
        Me.CMBpnlStatus.Size = New System.Drawing.Size(428, 80)
        Me.CMBpnlStatus.TabIndex = 8
        '
        'cbxSupenciónTemp
        '
        Me.cbxSupenciónTemp.AutoSize = True
        Me.cbxSupenciónTemp.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cbxSupenciónTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSupenciónTemp.Location = New System.Drawing.Point(12, 50)
        Me.cbxSupenciónTemp.Name = "cbxSupenciónTemp"
        Me.cbxSupenciónTemp.Size = New System.Drawing.Size(166, 19)
        Me.cbxSupenciónTemp.TabIndex = 10
        Me.cbxSupenciónTemp.Text = "Suspención Temporal"
        Me.cbxSupenciónTemp.UseVisualStyleBackColor = False
        '
        'cbxCancelado
        '
        Me.cbxCancelado.AutoSize = True
        Me.cbxCancelado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCancelado.Location = New System.Drawing.Point(150, 15)
        Me.cbxCancelado.Name = "cbxCancelado"
        Me.cbxCancelado.Size = New System.Drawing.Size(94, 19)
        Me.cbxCancelado.TabIndex = 2
        Me.cbxCancelado.Text = "Cancelado"
        Me.cbxCancelado.UseVisualStyleBackColor = True
        '
        'cbxSuspendido
        '
        Me.cbxSuspendido.AutoSize = True
        Me.cbxSuspendido.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cbxSuspendido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxSuspendido.Location = New System.Drawing.Point(308, 15)
        Me.cbxSuspendido.Name = "cbxSuspendido"
        Me.cbxSuspendido.Size = New System.Drawing.Size(102, 19)
        Me.cbxSuspendido.TabIndex = 4
        Me.cbxSuspendido.Text = "Suspendido"
        Me.cbxSuspendido.UseVisualStyleBackColor = False
        '
        'cbxActivos
        '
        Me.cbxActivos.AutoSize = True
        Me.cbxActivos.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cbxActivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxActivos.Location = New System.Drawing.Point(12, 15)
        Me.cbxActivos.Name = "cbxActivos"
        Me.cbxActivos.Size = New System.Drawing.Size(70, 19)
        Me.cbxActivos.TabIndex = 1
        Me.cbxActivos.Text = "Activos"
        Me.cbxActivos.UseVisualStyleBackColor = False
        '
        'CMBlblTitulo
        '
        Me.CMBlblTitulo.AutoSize = True
        Me.CMBlblTitulo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBlblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTitulo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBlblTitulo.Location = New System.Drawing.Point(18, 18)
        Me.CMBlblTitulo.Name = "CMBlblTitulo"
        Me.CMBlblTitulo.Size = New System.Drawing.Size(306, 24)
        Me.CMBlblTitulo.TabIndex = 7
        Me.CMBlblTitulo.Text = "Selecciona el Status de Cliente:"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(314, 154)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(22, 162)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 9
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'FrmSelStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 210)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.CMBpnlStatus)
        Me.Controls.Add(Me.CMBlblTitulo)
        Me.Name = "FrmSelStatus"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmSelStatus"
        Me.CMBpnlStatus.ResumeLayout(False)
        Me.CMBpnlStatus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBpnlStatus As System.Windows.Forms.Panel
    Friend WithEvents cbxSupenciónTemp As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCancelado As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSuspendido As System.Windows.Forms.CheckBox
    Friend WithEvents cbxActivos As System.Windows.Forms.CheckBox
    Friend WithEvents CMBlblTitulo As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class
