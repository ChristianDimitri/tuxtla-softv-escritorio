﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRepOrdSer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtpFechaIni = New System.Windows.Forms.DateTimePicker()
        Me.rbRAPAR = New System.Windows.Forms.RadioButton()
        Me.rbRETCA = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.gbFechas = New System.Windows.Forms.GroupBox()
        Me.Trabajo = New System.Windows.Forms.GroupBox()
        Me.gbFechas.SuspendLayout()
        Me.Trabajo.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.CustomFormat = "MMM / yyyy"
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaIni.Location = New System.Drawing.Point(57, 31)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(114, 21)
        Me.dtpFechaIni.TabIndex = 0
        '
        'rbRAPAR
        '
        Me.rbRAPAR.AutoSize = True
        Me.rbRAPAR.Checked = True
        Me.rbRAPAR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbRAPAR.Location = New System.Drawing.Point(77, 30)
        Me.rbRAPAR.Name = "rbRAPAR"
        Me.rbRAPAR.Size = New System.Drawing.Size(183, 19)
        Me.rbRAPAR.TabIndex = 2
        Me.rbRAPAR.TabStop = True
        Me.rbRAPAR.Text = "Retiro de Aparato Digital"
        Me.rbRAPAR.UseVisualStyleBackColor = True
        '
        'rbRETCA
        '
        Me.rbRETCA.AutoSize = True
        Me.rbRETCA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbRETCA.Location = New System.Drawing.Point(77, 55)
        Me.rbRETCA.Name = "rbRETCA"
        Me.rbRETCA.Size = New System.Drawing.Size(173, 19)
        Me.rbRETCA.TabIndex = 3
        Me.rbRETCA.Text = "Retiro de Cablemodem"
        Me.rbRETCA.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "de:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(177, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "a:"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.CustomFormat = "MMM / yyyy"
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaFin.Location = New System.Drawing.Point(202, 31)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(114, 21)
        Me.dtpFechaFin.TabIndex = 6
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(40, 209)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 7
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(182, 209)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 8
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'gbFechas
        '
        Me.gbFechas.Controls.Add(Me.dtpFechaIni)
        Me.gbFechas.Controls.Add(Me.dtpFechaFin)
        Me.gbFechas.Controls.Add(Me.Label1)
        Me.gbFechas.Controls.Add(Me.Label2)
        Me.gbFechas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFechas.Location = New System.Drawing.Point(12, 13)
        Me.gbFechas.Name = "gbFechas"
        Me.gbFechas.Size = New System.Drawing.Size(337, 69)
        Me.gbFechas.TabIndex = 9
        Me.gbFechas.TabStop = False
        Me.gbFechas.Text = "Periodo de tiempo"
        '
        'Trabajo
        '
        Me.Trabajo.Controls.Add(Me.rbRAPAR)
        Me.Trabajo.Controls.Add(Me.rbRETCA)
        Me.Trabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Trabajo.Location = New System.Drawing.Point(12, 88)
        Me.Trabajo.Name = "Trabajo"
        Me.Trabajo.Size = New System.Drawing.Size(337, 93)
        Me.Trabajo.TabIndex = 10
        Me.Trabajo.TabStop = False
        Me.Trabajo.Text = "Periodo de tiempo"
        '
        'FrmRepOrdSer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(364, 259)
        Me.Controls.Add(Me.Trabajo)
        Me.Controls.Add(Me.gbFechas)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Name = "FrmRepOrdSer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte de Retiro de Aparatos"
        Me.gbFechas.ResumeLayout(False)
        Me.gbFechas.PerformLayout()
        Me.Trabajo.ResumeLayout(False)
        Me.Trabajo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtpFechaIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbRAPAR As System.Windows.Forms.RadioButton
    Friend WithEvents rbRETCA As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents gbFechas As System.Windows.Forms.GroupBox
    Friend WithEvents Trabajo As System.Windows.Forms.GroupBox
End Class
